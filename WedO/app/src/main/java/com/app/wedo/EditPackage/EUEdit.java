package com.app.wedo.EditPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.wedo.MainActivity;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EUEdit extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "EUEdit";

    @NotEmpty
    private EditText email, username;

    private Button update;

    final Context context = this;

    private Validator validator;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_euedit);
        setTitle("Edit Email/Username");

        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.uname);

        update = (Button) findViewById(R.id.btn_eu_update);

        email.setText(SaveSharedPreference.getInstance(this).getEmail());
        username.setText(SaveSharedPreference.getInstance(this).getUsername());

        validator = new Validator(this);
        validator.setValidationListener(this);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Update Your Email/Username");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updateEUEdit();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });
    }

    private void updateEUEdit() {
        validator.validate();
        if (email.getText().toString().length() == 0 && username.getText().toString().length() == 0) {
            email.setError("Please Input Here!");
            username.setError("Please Input Here!");
        } else {
            final String id = SaveSharedPreference.getInstance(this).getId();
            final String semail = SaveSharedPreference.getInstance(this).getEmail();
            final String suname = SaveSharedPreference.getInstance(this).getUsername();
            final Api euapi = Retroserver.getClient().create(Api.class);
            final String aemail = email.getText().toString();
            final String auname = username.getText().toString();

            if((semail.equals(aemail)) && (suname.equals(auname))){
                Toast.makeText(EUEdit.this, "Email/Username Has Exist", Toast.LENGTH_LONG).show();
            } else {
                retrofit2.Call<ResponModel> update = euapi.updateeuname(id, aemail, auname);
                update.enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                        String kode = response.body().getKode();
                        String pesan = response.body().getPesan();
                        if (pesan.equals("Email/Username Has Exist")){
                            Toast.makeText(EUEdit.this, pesan, Toast.LENGTH_SHORT).show();
                        } else {
                            SaveSharedPreference.getInstance(EUEdit.this).setUpdateEmailUsername(
                                    aemail,
                                    auname);
                            Toast.makeText(EUEdit.this, pesan, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(EUEdit.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        Toast.makeText(EUEdit.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
