package com.app.wedo.EditPackage;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.wedo.ListSchedule;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.TimePickerFragment;
import com.app.wedo.model.ListScheduleModel;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LSEdit extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener,Validator.ValidationListener {

    private static final String TAG = "LSEdit";

    private Button ls_update;
    private ListScheduleModel listScheduleModels;

    @NotEmpty
    TextView date, time;
    EditText name, no_hp, event, place;
    private DatePickerDialog.OnDateSetListener DateListener;

    private String id, id_vendor;
    private Api lsapi;

    private Validator validator;

    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lsedit);
        setTitle("Edit Schedule");

        name = (EditText) findViewById(R.id.ls_name);
        no_hp = (EditText) findViewById(R.id.ls_no_hp);
        event = (EditText) findViewById(R.id.ls_event);
        place = (EditText) findViewById(R.id.ls_place);
        date = (TextView) findViewById(R.id.ls_date);
        time = (TextView) findViewById(R.id.ls_time);

        validator = new Validator(this);
        validator.setValidationListener(this);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "Time Picker");
            }
        });

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        id_vendor = intent.getStringExtra("id_vendor");

        name.setText(intent.getStringExtra("name"));
        no_hp.setText(intent.getStringExtra("no_hp"));
        event.setText(intent.getStringExtra("event"));
        place.setText(intent.getStringExtra("place"));
        date.setText(intent.getStringExtra("date"));
        time.setText(intent.getStringExtra("time"));

        ls_update = (Button) findViewById(R.id.btn_ls_done);
        ls_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Update Your Schedule");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updateLS();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        LSEdit.this,android.R.style.Theme_Holo_Dialog_MinWidth,
                        DateListener,
                        year,month,day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        DateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String datee = year + "/" + month + "/" + day;
                date.setText(datee);
            }
        };
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            TextView textView = (TextView) findViewById(R.id.ls_time);
            textView.setText(hourOfDay + ":" + minute + ":00");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete:
                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
                alertDialog.setTitle("Delete Schedule!");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteLS();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                android.app.AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateLS() {
        validator.validate();
        if (name.getText().toString().length() == 0 && no_hp.getText().toString().length() == 0 &&
                event.getText().toString().length() == 0 && place.getText().toString().length() == 0) {
            name.setError("Please Input Here!");
            no_hp.setError("Please Input Here!");
            event.setError("Please Input Here!");
            place.setError("Please Input Here!");
        } else {
            Intent intent = getIntent();
            final String id = intent.getStringExtra("id");
            final Api lsapi = Retroserver.getClient().create(Api.class);
            final String a_name = name.getText().toString();
            final String a_no_hp = no_hp.getText().toString();
            final String a_event = event.getText().toString();
            final String a_date = date.getText().toString();
            final String a_time = time.getText().toString();
            final String a_place = place.getText().toString();

            Call<ResponModel> update = lsapi.updateSchedule(id, a_name, a_no_hp, a_event, a_date, a_time, a_place);
            update.enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, retrofit2.Response<ResponModel> response) {
                    Toast.makeText(LSEdit.this, "Update Succes", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LSEdit.this, ListSchedule.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    Toast.makeText(LSEdit.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void deleteLS() {
        final Api lsapi = Retroserver.getClient().create(Api.class);
        lsapi.deleteSchedule(id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()){
                    Toast.makeText(LSEdit.this, "Succes Delete", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LSEdit.this, ListSchedule.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                } else {
                    Toast.makeText(LSEdit.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                Toast.makeText(LSEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
