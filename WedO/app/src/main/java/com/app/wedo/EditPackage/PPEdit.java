package com.app.wedo.EditPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.wedo.MainActivity;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.LogInVendorModel;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PPEdit extends AppCompatActivity {

    private static final String TAG = "PVEdit";

    @NotEmpty
    private ImageView new_picture, old_picture;

    private Button update, upload_image;

    private Validator validator;

    final Context context = this;

    private static final int IMG_REQUEST = 777;

    private Bitmap bitmap;

    private String id;

    private List<LogInVendorModel> logInVendorModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppedit);
        setTitle("Edit Photo Profile");

        update = (Button) findViewById(R.id.btn_pp_update);
        upload_image = (Button) findViewById(R.id.uploadpp);

        old_picture = (ImageView) findViewById(R.id.photo_profile);
        fetchProfilePicture();

        id = SaveSharedPreference.getInstance(this).getId();

        new_picture = (ImageView) findViewById(R.id.photo_profile2);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Update Your Profile");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updatePPEdit();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });

        upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void updatePPEdit() {
        if (bitmap == null) {
            Toast.makeText(PPEdit.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        } else {
            final String id = SaveSharedPreference.getInstance(this).getId();
            final Api ppapi = Retroserver.getClient().create(Api.class);
            final String picture = imageToString();
            retrofit2.Call<ResponModel> update = ppapi.updatephotoprofil(id, picture);
            update.enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    Toast.makeText(PPEdit.this, "Error", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    Toast.makeText(PPEdit.this, "Update Succes", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(PPEdit.this, MainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
            });
        }
    }


    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                new_picture.setImageBitmap(bitmap);
                new_picture.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(PPEdit.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    private String imageToString() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imgByte = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgByte, Base64.DEFAULT);
    }

    private void fetchProfilePicture() {
        Api api = Retroserver.getClient().create(Api.class);
        final String aid = SaveSharedPreference.getInstance(this).getId();
        Call<List<LogInVendorModel>> call = api.getProfilePicture(aid);
        call.enqueue(new Callback<List<LogInVendorModel>>() {
            @Override
            public void onResponse(Call<List<LogInVendorModel>> call, Response<List<LogInVendorModel>> response) {
                logInVendorModels = response.body();
                String pic = logInVendorModels.get(0).getPicture();
                byte[] decodeString = Base64.decode(pic, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                old_picture.setImageBitmap(bitmap);
            }

            @Override
            public void onFailure(Call<List<LogInVendorModel>> call, Throwable t) {
                Toast.makeText(PPEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
        /*Api api = Retroserver.getClient().create(Api.class);
        final String aid = SaveSharedPreference.getInstance(this).getId();
        Call<LogInVendorModel> call = api.getProfilePicture(aid);
        call.enqueue(new Callback<LogInVendorModel>() {
            @Override
            public void onResponse(Call<LogInVendorModel> call, Response<LogInVendorModel> response) {
                String pic = response.body().getPicture();

                byte[] decodeString = Base64.decode(pic, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                old_picture.setImageBitmap(bitmap);
            }

            @Override
            public void onFailure(Call<LogInVendorModel> call, Throwable t) {
                Toast.makeText(PPEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });*/
    }
}
