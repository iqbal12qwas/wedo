package com.app.wedo.EditPackage;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.MainActivity;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.LogInVendorModel;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PVEdit extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "PVEdit";

    @NotEmpty
    private EditText ETvname, ETnohp, ETvaddress, ETname;

    private Button update;

    private ImageView picture;

    private Spinner ETprovinsi;

    private TextView TVbirthday, TVChangePP;

    private Validator validator;

    private DatePickerDialog.OnDateSetListener DateListener;

    final Context context = this;

    private static final int IMG_REQUEST = 777;

    private Bitmap bitmap;

    private String id;

    private List<LogInVendorModel> logInVendorModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pvedit);
        setTitle("Edit Profile");

        ETprovinsi = (Spinner) findViewById(R.id.provinsi);

        ETvname = (EditText) findViewById(R.id.vname);
        ETnohp = (EditText) findViewById(R.id.password);
        ETvaddress = (EditText) findViewById(R.id.address);
        ETname = (EditText) findViewById(R.id.name);

        TVbirthday = (TextView) findViewById(R.id.birthday);
        TVChangePP = (TextView) findViewById(R.id.tv_changepp);

        update = (Button) findViewById(R.id.btn_eu_update);

        picture = (ImageView) findViewById(R.id.photo_profile);
        fetchProfilePicture();

        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PVEdit.this, PPEdit.class);
                startActivity(intent);
            }
        });

        TVChangePP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PVEdit.this, PPEdit.class);
                startActivity(intent);
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);
        id = SaveSharedPreference.getInstance(this).getId();
        ETvname.setText(SaveSharedPreference.getInstance(this).getVendorName());
        ETnohp.setText(SaveSharedPreference.getInstance(this).getNoHp());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.a_provinsi, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ETprovinsi.setAdapter(adapter);
        String spinnerValue = SaveSharedPreference.getInstance(this).getProvinsi();
        int spinnerPos = adapter.getPosition(spinnerValue);
        ETprovinsi.setSelection(spinnerPos);

        ETprovinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ETvaddress.setText(SaveSharedPreference.getInstance(this).getVendorAddress());
        ETname.setText(SaveSharedPreference.getInstance(this).getName());
        TVbirthday.setText(SaveSharedPreference.getInstance(this).getBirthdayDate());

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Update Your Profile");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updatePVEdit();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });


        TVbirthday.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PVEdit.this, android.R.style.Theme_Holo_Dialog_MinWidth,
                        DateListener,
                        year, month, day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        DateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String datee = year + "/" + month + "/" + day;
                TVbirthday.setText(datee);
            }
        };
    }

    private void updatePVEdit() {
        validator.validate();
        if (ETnohp.getText().toString().length() == 0 && ETvaddress.getText().toString().length() == 0 &&
                ETname.getText().toString().length() == 0) {
            ETvname.setError("Please Input Here!");
            ETnohp.setError("Please Input Here!");
            ETvaddress.setError("Please Input Here!");
            ETname.setError("Please Input Here!");
        } else {
            final String id = SaveSharedPreference.getInstance(this).getId();
            final Api pvapi = Retroserver.getClient().create(Api.class);
            final String name = ETname.getText().toString();
            final String birthday = TVbirthday.getText().toString();
            final String vendorname = ETvname.getText().toString();
            final String no_hp = ETnohp.getText().toString();
            final String vaddress = ETvaddress.getText().toString();
            final String provinsi = ETprovinsi.getSelectedItem().toString();
            retrofit2.Call<ResponModel> update = pvapi.updateprofil(id, name, birthday, vendorname, no_hp, vaddress, provinsi);
            update.enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SaveSharedPreference.getInstance(PVEdit.this).setUpdateProfile(
                            name,
                            birthday,
                            vendorname,
                            no_hp,
                            vaddress,
                            provinsi);
                    Log.d("aasas", vendorname);
                    Log.d("aasas", name);
                    Toast.makeText(PVEdit.this, "Update Succes", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(PVEdit.this, MainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    Toast.makeText(PVEdit.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void fetchProfilePicture() {
        Api api = Retroserver.getClient().create(Api.class);
        final String aid = SaveSharedPreference.getInstance(this).getId();
        Call<List<LogInVendorModel>> call = api.getProfilePicture(aid);
        call.enqueue(new Callback<List<LogInVendorModel>>() {
            @Override
            public void onResponse(Call<List<LogInVendorModel>> call, Response<List<LogInVendorModel>> response) {
                logInVendorModels = response.body();
                String pic = logInVendorModels.get(0).getPicture();
                byte[] decodeString = Base64.decode(pic, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                picture.setImageBitmap(bitmap);
            }

            @Override
            public void onFailure(Call<List<LogInVendorModel>> call, Throwable t) {
                Toast.makeText(PVEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
        /*Api api = Retroserver.getClient().create(Api.class);
        final String aid = SaveSharedPreference.getInstance(this).getId();
        Call<LogInVendorModel> call = api.getProfilePicture(aid);
        call.enqueue(new Callback<LogInVendorModel>() {
            @Override
            public void onResponse(Call<LogInVendorModel> call, Response<LogInVendorModel> response) {
                String pic = response.body().getPicture();

                byte[] decodeString = Base64.decode(pic, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                picture.setImageBitmap(bitmap);
            }

            @Override
            public void onFailure(Call<LogInVendorModel> call, Throwable t) {
                Toast.makeText(PVEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });*/
    }

}
