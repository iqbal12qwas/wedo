package com.app.wedo.EditPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.app.wedo.MainActivity;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PWEdit extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "EUEdit";

    @NotEmpty
    private EditText password, confirm_password;

    private Button update;

    private CheckBox showhide;

    final Context context = this;

    private Validator validator;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pwedit);
        setTitle("Edit Password");

        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);

        showhide = (CheckBox) findViewById(R.id.show_password);

        update = (Button) findViewById(R.id.btn_eu_update);
        password.setText(SaveSharedPreference.getInstance(this).getPassword());

        validator = new Validator(this);
        validator.setValidationListener(this);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Update Your Password");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (password.getText().toString().equals(confirm_password.getText().toString())){
                                    updatePWEdit();
                                } else {
                                    Toast.makeText(PWEdit.this, "Password & Confirm Password Not Same", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });

        showhide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }

    private void updatePWEdit() {
        validator.validate();
        if (password.getText().toString().length() == 0 && confirm_password.getText().toString().length() == 0) {
            password.setError("Please Input Here!");
            confirm_password.setError("Please Input Here!");
        } else {
            final String id = SaveSharedPreference.getInstance(this).getId();
            final Api pwapi = Retroserver.getClient().create(Api.class);
            final String apassword = password.getText().toString();

            retrofit2.Call<ResponModel> update = pwapi.updatepassword(id, apassword);
            update.enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    Toast.makeText(PWEdit.this, "Update Succes", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(PWEdit.this, MainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    Toast.makeText(PWEdit.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
