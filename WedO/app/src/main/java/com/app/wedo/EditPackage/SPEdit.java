package com.app.wedo.EditPackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.ListItemOrPackage;
import com.app.wedo.R;
import com.app.wedo.adapter.ListReviewAdapter;
import com.app.wedo.adapter.SPPictureAdapter;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ListItemModel;
import com.app.wedo.model.ListReviewModel;
import com.app.wedo.model.ResponModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SPEdit extends AppCompatActivity {

    private RecyclerView recyclerView, recyclerView1;
    private RecyclerView.LayoutManager layoutManager, layoutManager1;
    private List<ListItemModel> listItemModels;
    private List<ListReviewModel> listReviewModel;
    private SPPictureAdapter adapter;
    private ListReviewAdapter adapter1;
    private Api api;
    ProgressBar progressBar, progressBar1;

    TextView option, type, category, name, price, description, category_package, pax, stock;
    RatingBar ratingg;
    private String id, id_vendor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spedit);
        setTitle("Your Item / Package");

        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.sp_picture);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        progressBar1 = findViewById(R.id.progress2);
        recyclerView1 = findViewById(R.id.sp_review);
        layoutManager1 = new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(layoutManager1);
        recyclerView1.setHasFixedSize(true);

        type = (TextView) findViewById(R.id.sp_type);
        category = (TextView) findViewById(R.id.sp_category);
        pax = (TextView) findViewById(R.id.pax);
        category_package = (TextView) findViewById(R.id.sp_c_package);
        name = (TextView) findViewById(R.id.sp_name);
        price = (TextView) findViewById(R.id.sp_price);
        option = (TextView) findViewById(R.id.sp_option);
        description = (TextView) findViewById(R.id.sp_description);
        stock = (TextView) findViewById(R.id.stock);
        ratingg = (RatingBar) findViewById(R.id.ratingBar);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        id_vendor = intent.getStringExtra("id_vendor");

        type.setText(intent.getStringExtra("type"));
        name.setText(intent.getStringExtra("name"));
        description.setText(intent.getStringExtra("description"));
        category.setText(intent.getStringExtra("category"));
        pax.setText(intent.getStringExtra("pax"));
        stock.setText(intent.getStringExtra("stock"));
        category_package.setText(intent.getStringExtra("category_package"));
        option.setText(intent.getStringExtra("option"));
        price.setText("Rp. " + intent.getStringExtra("price"));

        final String id_product = id;
        final String id_vendors = id_vendor;
        final Api api = Retroserver.getClient().create(Api.class);

        retrofit2.Call<ResponModel> rating = api.getrating(id_product, id_vendors);
        rating.enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                String pesan = response.body().getPesan();
                Float r = Float.valueOf(0);
                if (pesan != null){
                    r = Float.parseFloat(pesan);
                }

                ratingg.setRating(r);
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                Toast.makeText(SPEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });

        /*value_rating.setText(intent.getStringExtra("rating"));

        String rate = value_rating.getText().toString();
        Float r = Float.parseFloat(rate);

        ratingg.setRating(r);*/

        fetchpicture();

        fetchreview();

    }

    private void fetchreview() {
        api = Retroserver.getClient().create(Api.class);
        final String aid_vendor = SaveSharedPreference.getInstance(this).getId();
        final String aid = id;
        Call<List<ListReviewModel>> call = api.getReview(aid, aid_vendor);
        call.enqueue(new Callback<List<ListReviewModel>>() {
            @Override
            public void onResponse(Call<List<ListReviewModel>> call, Response<List<ListReviewModel>> response) {
                progressBar1.setVisibility(View.GONE);
                listReviewModel = response.body();
                adapter1 = new ListReviewAdapter(listReviewModel, SPEdit.this);
                recyclerView1.setAdapter(adapter1);
                adapter1.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ListReviewModel>> call, Throwable t) {
                Toast.makeText(SPEdit.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchpicture() {
        api = Retroserver.getClient().create(Api.class);
        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        final String id_vendor = intent.getStringExtra("id_vendor");
        Call<List<ListItemModel>> call = api.getSPPictureModel(id, id_vendor);
        call.enqueue(new Callback<List<ListItemModel>>() {
            @Override
            public void onResponse(Call<List<ListItemModel>> call, Response<List<ListItemModel>> response) {
                progressBar.setVisibility(View.GONE);
                listItemModels = response.body();
                adapter = new SPPictureAdapter(listItemModels, SPEdit.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ListItemModel>> call, Throwable t) {
                Toast.makeText(SPEdit.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SPEdit.this, ListItemOrPackage.class));
        finish();
    }
}
