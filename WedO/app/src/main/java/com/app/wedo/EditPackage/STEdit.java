package com.app.wedo.EditPackage;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.common.ProofOfPayment;
import com.app.wedo.model.ResponModel;

import java.util.Calendar;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class STEdit extends AppCompatActivity {

    private static final String TAG = "STEdit";
    private Button st_update;

    TextView tv_name;
    TextView tv_no_hp;
    TextView tv_order;
    TextView tv_category;
    TextView tv_price;
    TextView tv_type;
    TextView tv_booking;
    TextView tv_minus;
    TextView tv_total;
    TextView tv_option;
    TextView tv_paid_before;
    TextView tv_unit;
    TextView tv_value;
    TextView tv_status;

    EditText tv_note;
    EditText tv_paid;

    private DatePickerDialog.OnDateSetListener stDateListener;

    private String id, id_product, id_customer, id_vendor;

    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stedit);
        setTitle("Edit Sell Transaction");

        st_update = (Button) findViewById(R.id.st_update);

        tv_name = (TextView) findViewById(R.id.st_name);
        tv_no_hp = (TextView) findViewById(R.id.st_no_hp);
        tv_type = (TextView) findViewById(R.id.st_type);
        tv_order = (TextView) findViewById(R.id.st_order);
        tv_category = (TextView) findViewById(R.id.st_category);
        tv_option = (TextView) findViewById(R.id.st_option);
        tv_price = (TextView) findViewById(R.id.st_price);
        tv_booking = (TextView) findViewById(R.id.st_date);
        tv_minus = (TextView) findViewById(R.id.st_residue);
        tv_total = (TextView) findViewById(R.id.st_total);
        tv_unit = (TextView) findViewById(R.id.st_unit);
        tv_paid_before = (TextView) findViewById(R.id.st_paid_before);
        tv_status = (TextView) findViewById(R.id.st_status);

        tv_value = (TextView) findViewById(R.id.st_value);

        tv_note = (EditText) findViewById(R.id.st_note);
        tv_paid = (EditText) findViewById(R.id.st_paid);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        id_customer = intent.getStringExtra("id_customer");
        id_product = intent.getStringExtra("id_product");
        id_vendor = intent.getStringExtra("id_vendor");


        if (intent != null) {
            st_update.setVisibility(View.VISIBLE);
            tv_name.setText(intent.getStringExtra("name"));
            tv_no_hp.setText(intent.getStringExtra("no_hp"));
            tv_type.setText(intent.getStringExtra("type"));
            tv_option.setText(intent.getStringExtra("option"));
            tv_order.setText(intent.getStringExtra("order"));
            tv_category.setText(intent.getStringExtra("category"));
            tv_booking.setText(intent.getStringExtra("booking"));
            tv_price.setText(intent.getStringExtra("price"));
            tv_note.setText(intent.getStringExtra("note"));
            tv_unit.setText(intent.getStringExtra("unit"));
            tv_value.setText(intent.getStringExtra("paid"));
            tv_paid_before.setText(intent.getStringExtra("paid"));
            tv_minus.setText(intent.getStringExtra("minus"));
            tv_total.setText(intent.getStringExtra("total"));
            tv_status.setText(intent.getStringExtra("status"));


            tv_paid.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tv_paid_before.setText(sumNumbers());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            tv_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tv_paid_before.setText(sumNumbers());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            tv_paid_before.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tv_minus.setText(minNumbers());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            tv_total.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tv_minus.setText(minNumbers());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }

        st_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String minus = tv_minus.getText().toString().trim();
                if (Integer.valueOf(minus) >= 0){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Confirm!");
                    alertDialog.setMessage("Are you sure to update ?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    updateST();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog.show();
                } else {
                    Toast.makeText(STEdit.this, "Minus can't be negative number", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_booking.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        STEdit.this, android.R.style.Theme_Holo_Dialog_MinWidth,
                        stDateListener,
                        year, month, day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        stDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String date = year + "/" + month + "/" + day;
                tv_booking.setText(date);
            }
        };

        if (tv_status.getText().toString().equals("Completed")){
            st_update.setEnabled(false);
            tv_booking.setEnabled(false);
            tv_note.setEnabled(false);
            tv_paid.setEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                if (tv_paid_before.getText().toString().equals(tv_total.getText().toString())) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Send Data Service/Product Transaction?");
                    alertDialog.setMessage("Are you sure ?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    senddataLA();
                                    deleteLA();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog.show();
                } else {
                    Toast.makeText(STEdit.this, "Finish Your Transaction Until Minus = 0", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.delete:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Delete This Acquittance");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final Api laapi = Retroserver.getClient().create(Api.class);
                                final String unit = tv_unit.getText().toString();
                                laapi.deleteListAcquittance2(id, id_product, unit).enqueue(new Callback<ResponModel>() {
                                    @Override
                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                        if (response.isSuccessful()) {
                                            Toast.makeText(STEdit.this, "Delete Succes", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(STEdit.this, ListAcquittance.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        } else {
                                            Toast.makeText(STEdit.this, "Failed", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                        Toast.makeText(STEdit.this, "Error", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
                break;
            case R.id.payment:
                final String name = tv_name.getText().toString();
                final String no_hp = tv_no_hp.getText().toString();
                final String type = tv_type.getText().toString();
                final String option = tv_option.getText().toString();
                final String order = tv_order.getText().toString();
                final String category = tv_booking.getText().toString();
                Intent intent = new Intent(STEdit.this , ProofOfPayment.class);
                intent.putExtra("id", id);
                intent.putExtra("id_product", id_product);
                intent.putExtra("id_customer", id_customer);
                intent.putExtra("id_vendor", id_vendor);
                intent.putExtra("name", name);
                intent.putExtra("no_hp", no_hp);
                intent.putExtra("order", order);
                intent.putExtra("category", category);
                intent.putExtra("type", type);
                intent.putExtra("option", option);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteLA() {
        final Api rtapi = Retroserver.getClient().create(Api.class);
        rtapi.deleteListAcquittance(id, id_product).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()) {
                    startActivity(new Intent(STEdit.this, ListAcquittance.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                } else {
                    Toast.makeText(STEdit.this, "Fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                Toast.makeText(STEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private String minNumbers() {
        long number1, number3;
        if (tv_paid_before.getText().toString() != "" && tv_paid_before.getText().length() > 0) {
            number1 = Long.parseLong(tv_paid_before.getText().toString());
        } else {
            number1 = 0;
        }

        if (tv_total.getText().toString() != "" && tv_total.getText().length() > 0) {
            number3 = Long.parseLong(tv_total.getText().toString());
        } else {
            number3 = 0;
        }
        return Long.toString(number3 - number1);
    }

    private String sumNumbers() {
        long number1, number2;
        if (tv_paid.getText().toString() != "" && tv_paid.getText().length() > 0) {
            number1 = Long.parseLong(tv_paid.getText().toString());
        } else {
            number1 = 0;
        }

        if (tv_value.getText().toString() != "" && tv_value.getText().length() > 0) {
            number2 = Long.parseLong(tv_value.getText().toString());
        } else {
            number2 = 0;
        }

        return Long.toString(number1 + number2);
    }

    private void updateST() {
        if (tv_minus.getText().toString().equals("0")){
            Intent intent = getIntent();
            final String id = intent.getStringExtra("id");
            final String id_customer = intent.getStringExtra("id_customer");
            final String id_product = intent.getStringExtra("id_product");
            final String id_vendor = intent.getStringExtra("id_vendor");
            String name = tv_name.getText().toString();
            String no_hp = tv_no_hp.getText().toString();
            String type = tv_type.getText().toString();
            String order = tv_order.getText().toString();
            String category = tv_category.getText().toString();
            String option = tv_option.getText().toString();
            String booking = tv_booking.getText().toString();
            String price = tv_price.getText().toString();
            String unit = tv_unit.getText().toString();
            String note = tv_note.getText().toString();
            String paid_before = tv_paid_before.getText().toString();
            String minus = tv_minus.getText().toString();
            String total = tv_total.getText().toString();
            String status = "Completed";

            final Api stapi = Retroserver.getClient().create(Api.class);
            Call<ResponModel> update = stapi.updateSellTransaction(id, id_customer, id_product, id_vendor, name, no_hp, order, category, option, booking, price, type, unit, note, paid_before, minus, total, status);
            update.enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, retrofit2.Response<ResponModel> response) {
                    Toast.makeText(STEdit.this, "Update Succes", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(STEdit.this, ListAcquittance.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    Toast.makeText(STEdit.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Intent intent = getIntent();
            final String id = intent.getStringExtra("id");
            final String id_customer = intent.getStringExtra("id_customer");
            final String id_product = intent.getStringExtra("id_product");
            final String id_vendor = intent.getStringExtra("id_vendor");
            final String paxx = intent.getStringExtra("pax");
            final String tot_pax = intent.getStringExtra("total_pax");
            String name = tv_name.getText().toString();
            String no_hp = tv_no_hp.getText().toString();
            String type = tv_type.getText().toString();
            String order = tv_order.getText().toString();
            String category = tv_category.getText().toString();
            String option = tv_option.getText().toString();
            String booking = tv_booking.getText().toString();
            String price = tv_price.getText().toString();
            String unit = tv_unit.getText().toString();
            String note = tv_note.getText().toString();
            String paid_before = tv_paid_before.getText().toString();
            String minus = tv_minus.getText().toString();
            String total = tv_total.getText().toString();
            String status = tv_status.getText().toString();

            final Api stapi = Retroserver.getClient().create(Api.class);
            Call<ResponModel> update = stapi.updateSellTransaction(id, id_customer, id_product, id_vendor, name, no_hp, order, category, option, booking, price, type, unit, note, paid_before, minus, total, status);
            update.enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, retrofit2.Response<ResponModel> response) {
                    Toast.makeText(STEdit.this, "Update Succes", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(STEdit.this, ListAcquittance.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    Toast.makeText(STEdit.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void senddataLA() {
        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        final Api stapi = Retroserver.getClient().create(Api.class);
        final String id_product = intent.getStringExtra("id_product");
        final String id_customer = intent.getStringExtra("id_customer");
        final String id_vendor = intent.getStringExtra("id_vendor");
        final String name = tv_name.getText().toString();
        final String no_hp = tv_no_hp.getText().toString();
        final String type = tv_type.getText().toString();
        final String order = tv_order.getText().toString();
        final String category = tv_category.getText().toString();
        final String option = tv_option.getText().toString();
        final String booking = tv_booking.getText().toString();
        final String price = tv_price.getText().toString();
        final String unit = tv_unit.getText().toString();
        final String note = tv_note.getText().toString();
        final String paid = tv_paid_before.getText().toString();
        final String total = tv_total.getText().toString();

        Call<ResponModel> sendST = stapi.sendDataST(id, id_product, id_customer, id_vendor, name, no_hp, type, order, category, option, booking, price, unit, note, paid, total);
        sendST.enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, retrofit2.Response<ResponModel> response) {
                Toast.makeText(STEdit.this, "Succes", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(STEdit.this, ListAcquittance.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                Toast.makeText(STEdit.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}
