package com.app.wedo.EditPackage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.R;
import com.app.wedo.YourCustomers;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ResponModel;
import com.app.wedo.model.YourCustomerModel;
import com.app.wedo.transaction.PackageTransaction;
import com.app.wedo.transaction.RentTransaction;
import com.app.wedo.transaction.SellTransaction;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YCEdit extends AppCompatActivity {

    private Button yc_delete, yc_transaction;
    private YourCustomerModel yourCustomerModels;

    TextView tv_name;
    TextView tv_no_hp;
    TextView tv_order;
    TextView tv_category;
    TextView tv_date_order;
    TextView tv_type;
    TextView tv_price;
    TextView tv_option;
    TextView tv_type_duration;
    TextView tv_status;

    private String your_customer_id, id_customer, id_vendor, id_product;
    private Api ycapi;



    final Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ycedit);
        setTitle("Your Customer Detail");

        tv_name = (TextView) findViewById(R.id.ls_name);
        tv_no_hp = (TextView) findViewById(R.id.ls_no_hp);
        tv_order = (TextView) findViewById(R.id.ls_place);
        tv_category = (TextView) findViewById(R.id.ls_event);
        tv_date_order = (TextView) findViewById(R.id.yce_date_order);
        tv_type = (TextView) findViewById(R.id.yce_type);
        tv_price = (TextView) findViewById(R.id.yce_price);
        tv_option = (TextView) findViewById(R.id.yce_option);
        tv_type_duration = (TextView) findViewById(R.id.yce_type_duration);
        tv_status = (TextView) findViewById(R.id.yce_status);

        Intent intent = getIntent();
        your_customer_id = intent.getStringExtra("id");
        id_product = intent.getStringExtra("id_product");
        id_customer = intent.getStringExtra("id_customer");
        id_vendor = intent.getStringExtra("id_vendor");
        tv_name.setText(intent.getStringExtra("name"));
        tv_no_hp.setText(intent.getStringExtra("no_hp"));
        tv_order.setText(intent.getStringExtra("order"));
        tv_category.setText(intent.getStringExtra("category"));
        tv_date_order.setText(intent.getStringExtra("date_order"));
        tv_price.setText(intent.getStringExtra("price"));
        tv_type.setText(intent.getStringExtra("type"));
        tv_option.setText(intent.getStringExtra("option"));
        tv_type_duration.setText(intent.getStringExtra("type_duration"));
        tv_status.setText(intent.getStringExtra("status"));

        final String name = intent.getStringExtra("name");
        final String no_hp = intent.getStringExtra("no_hp");
        final String order = intent.getStringExtra("order");
        final String category = intent.getStringExtra("category");
        final String price = intent.getStringExtra("price");
        final String type = intent.getStringExtra("type");
        final String option = intent.getStringExtra("option");
        final String type_duration = intent.getStringExtra("type_duration");
        final String pax = intent.getStringExtra("pax");
        final String stock = intent.getStringExtra("stock");

        ycapi = Retroserver.getClient().create(Api.class);
        yc_delete = (Button) findViewById(R.id.btn_yce_delete);
        yc_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Delete Your Customer Booking");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteYC();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });

        yc_transaction = (Button) findViewById(R.id.btn_yce_transaction);
        yc_transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Go Transaction");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (tv_type.getText().toString().equals("Rent")){
                                    Intent intent = new Intent(YCEdit.this , RentTransaction.class);
                                    intent.putExtra("id_product", id_product);
                                    intent.putExtra("id_customer", id_customer);
                                    intent.putExtra("id_vendor", id_vendor);
                                    intent.putExtra("name", name);
                                    intent.putExtra("no_hp", no_hp);
                                    intent.putExtra("order", order);
                                    intent.putExtra("category", category);
                                    intent.putExtra("price", price);
                                    intent.putExtra("type", type);
                                    intent.putExtra("option", option);
                                    intent.putExtra("type_duration", type_duration);
                                    intent.putExtra("pax", pax);
                                    v.getContext().startActivity(intent);
                                } else if (tv_type.getText().toString().equals("Sell")) {
                                    Intent intent = new Intent(YCEdit.this, SellTransaction.class);
                                    intent.putExtra("id_product", id_product);
                                    intent.putExtra("id_customer", id_customer);
                                    intent.putExtra("id_vendor", id_vendor);
                                    intent.putExtra("name", name);
                                    intent.putExtra("no_hp", no_hp);
                                    intent.putExtra("order", order);
                                    intent.putExtra("category", category);
                                    intent.putExtra("price", price);
                                    intent.putExtra("type", type);
                                    intent.putExtra("option", option);
                                    intent.putExtra("pax", pax);
                                    intent.putExtra("stock", stock);
                                    v.getContext().startActivity(intent);
                                } else {
                                    Intent intent = new Intent(YCEdit.this, PackageTransaction.class);
                                    intent.putExtra("id_product", id_product);
                                    intent.putExtra("id_customer", id_customer);
                                    intent.putExtra("id_vendor", id_vendor);
                                    intent.putExtra("name", name);
                                    intent.putExtra("no_hp", no_hp);
                                    intent.putExtra("order", order);
                                    intent.putExtra("category", category);
                                    intent.putExtra("price", price);
                                    intent.putExtra("type", type);
                                    intent.putExtra("option", option);
                                    intent.putExtra("pax", pax);
                                    v.getContext().startActivity(intent);
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
            }
        });
    }

    private void deleteYC() {
                ycapi.deleteYourCustomer(your_customer_id).enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                        if (response.isSuccessful()){
                            Toast.makeText(YCEdit.this, "Delete Succes", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(YCEdit.this, YourCustomers.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            Toast.makeText(YCEdit.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        Toast.makeText(YCEdit.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
