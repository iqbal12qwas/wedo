package com.app.wedo;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.app.wedo.adapter.ListAcquittanceAdapter;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ListAcquittanceModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAcquittance extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ListAcquittanceModel> listAcquittanceModel;
    private ListAcquittanceAdapter adapter;
    private Api api;
    ProgressBar progressBar;

    SwipeRefreshLayout swipeRefreshLayout;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_acquittance);
        setTitle("List Acquittance");

        id = SaveSharedPreference.getInstance(this).getId();

        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.LArecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.listacquittance_swipeup);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchLAcquittance(""); //without keyboard

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        fetchLAcquittance(""); //without keyboard
    }



    private void fetchLAcquittance(String key) {
        api = Retroserver.getClient().create(Api.class);
        final String id_vendor = id;
        Call<List<ListAcquittanceModel>> call = api.getListAcquittance(id_vendor, key);
        call.enqueue(new Callback<List<ListAcquittanceModel>>() {
            @Override
            public void onResponse(Call<List<ListAcquittanceModel>> call, Response<List<ListAcquittanceModel>> response) {
                progressBar.setVisibility(View.GONE);
                listAcquittanceModel = response.body();
                adapter = new ListAcquittanceAdapter(listAcquittanceModel, ListAcquittance.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ListAcquittanceModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListAcquittance.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        assert searchManager != null;
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchLAcquittance(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchLAcquittance(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ListAcquittance.this, MainActivity.class));
        finish();
    }
}
