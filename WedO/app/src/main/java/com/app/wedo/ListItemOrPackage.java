package com.app.wedo;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.adapter.ListItemAdapter;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ListItemModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListItemOrPackage extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ListItemModel> listItemModels;
    private ListItemAdapter adapter;
    private Api api;
    ProgressBar progressBar;

    Button button;
    SwipeRefreshLayout swipeRefreshLayout;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_service_product);
        setTitle("List Item / Package");

        progressBar = findViewById(R.id.progress3);
        recyclerView = findViewById(R.id.MSPrecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.itemorpackage_swipeup);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchItem(""); //without keyboard

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        checkFirstRun();

        fetchItem(""); //without keyboard
}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        assert searchManager != null;
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchItem(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchItem(newText);
                return false;
            }
        });
        return true;
    }

    public void checkFirstRun() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ListItemOrPackage.this);
            View view = LayoutInflater.from(ListItemOrPackage.this).inflate(R.layout.popup_firstrun_itemandpackage, null);

            TextView title = (TextView) view.findViewById(R.id.popup_tv);
            ImageView imageButton = (ImageView) view.findViewById(R.id.popup_image);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.popup_checkbox);

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                            .edit()
                            .putBoolean("isFirstRun", false)
                            .apply();
                }
            });

            builder.setView(view);
            builder.show();
        }
    }


    private void fetchItem(String key) {
        api = Retroserver.getClient().create(Api.class);
        final String id_vendor = SaveSharedPreference.getInstance(this).getId();
        Call<List<ListItemModel>> call = api.getItem(id_vendor, key);
        call.enqueue(new Callback<List<ListItemModel>>() {
            @Override
            public void onResponse(Call<List<ListItemModel>> call, Response<List<ListItemModel>> response) {
                progressBar.setVisibility(View.GONE);
                listItemModels = response.body();
                adapter = new ListItemAdapter(listItemModels, ListItemOrPackage.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ListItemModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListItemOrPackage.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ListItemOrPackage.this, MainActivity.class));
        finish();
    }
}


