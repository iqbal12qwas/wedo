package com.app.wedo;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.app.wedo.adapter.ListScheduleAdapter;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ListScheduleModel;
import com.app.wedo.popup.AddListSchedule;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListSchedule extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ListScheduleModel> listScheduleModels;
    private ListScheduleAdapter adapter;
    private Api api;
    ProgressBar progressBar;

    private Button add;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduling);
        setTitle("List Schedule");

        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.LSrecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        swipeRefreshLayout = findViewById(R.id.scheduling_swipeup);

        add = (Button) findViewById(R.id.ls_add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListSchedule.this, AddListSchedule.class));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchLSchedule(""); //without keyboard

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        fetchLSchedule("");
    }

    private void fetchLSchedule(String key) {
        api = Retroserver.getClient().create(Api.class);
        final String id_vendor = SaveSharedPreference.getInstance(this).getId();
        Call<List<ListScheduleModel>> call = api.getListSchedule(id_vendor, key);
        call.enqueue(new Callback<List<ListScheduleModel>>() {
            @Override
            public void onResponse(Call<List<ListScheduleModel>> call, Response<List<ListScheduleModel>> response) {
                progressBar.setVisibility(View.GONE);
                listScheduleModels = response.body();
                adapter = new ListScheduleAdapter(listScheduleModels, ListSchedule.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ListScheduleModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ListSchedule.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchLSchedule(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchLSchedule(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ListSchedule.this, MainActivity.class));
        finish();
    }

}
