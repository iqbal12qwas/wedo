package com.app.wedo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.EditPackage.EUEdit;
import com.app.wedo.EditPackage.PVEdit;
import com.app.wedo.EditPackage.PWEdit;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.LogInVendorModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;

    ImageButton IBManageServiceOrProduct, IBYourCostumer, ILAcquittance, ILSchedule;
    TextView TVManageServiceOrProduct, TVYourCostumer, TVLAAcquittance, TVLSchedule, TVName, TVEmail;
    ImageView IMGPicture;

    boolean doubleBackToExitPressedOnce = false;

    final Context context = this;

    private List<LogInVendorModel> logInVendorModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Home");

        ILAcquittance = (ImageButton) findViewById(R.id.lga);
        IBYourCostumer = (ImageButton) findViewById(R.id.yc);
        IBManageServiceOrProduct = (ImageButton) findViewById(R.id.msp);
        ILSchedule = (ImageButton) findViewById(R.id.ls);

        TVLAAcquittance = (TextView) findViewById(R.id.la);
        TVManageServiceOrProduct = (TextView) findViewById(R.id.tyc);
        TVYourCostumer = (TextView) findViewById(R.id.tmsp);
        TVLSchedule = (TextView) findViewById(R.id.tls);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        TVEmail = (TextView) header.findViewById(R.id.email);
        TVName = (TextView) header.findViewById(R.id.name);
        IMGPicture = (ImageView) header.findViewById(R.id.picture_dashboard);
        fetchProfilePicture();
        IMGPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PVEdit.class);
                startActivity(intent);
            }
        });

        TVEmail.setText(SaveSharedPreference.getInstance(this).getEmail());
        TVName.setText(SaveSharedPreference.getInstance(this).getName());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        IBManageServiceOrProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityMsp = new Intent(MainActivity.this, ListItemOrPackage.class);
                startActivity(ActivityMsp);
            }
        });

        IBYourCostumer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityYC = new Intent(MainActivity.this, YourCustomers.class);
                startActivity(ActivityYC);
            }
        });

        ILAcquittance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityLA = new Intent(MainActivity.this, ListAcquittance.class);
                startActivity(ActivityLA);
            }
        });

        ILSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityLS = new Intent(MainActivity.this, ListSchedule.class);
                startActivity(ActivityLS);
            }
        });

        TVLAAcquittance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityLA = new Intent(MainActivity.this, ListAcquittance.class);
                startActivity(ActivityLA);
            }
        });

        TVYourCostumer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityYC = new Intent(MainActivity.this, YourCustomers.class);
                startActivity(ActivityYC);
            }
        });

        TVManageServiceOrProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityMsp = new Intent(MainActivity.this, ListItemOrPackage.class);
                startActivity(ActivityMsp);
            }
        });

        TVLSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ActivityLs = new Intent(MainActivity.this, ListSchedule.class);
                startActivity(ActivityLs);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.home:
                Intent h = new Intent(MainActivity.this,MainActivity.class);
                startActivity(h);
                break;
            case R.id.lsche:
                Intent ls = new Intent(MainActivity.this, ListSchedule.class);
                startActivity(ls);
                break;
            case R.id.managesp:
                Intent msp = new Intent(MainActivity.this, ListItemOrPackage.class);
                startActivity(msp);
                break;
            case R.id.yourcostumer:
                Intent yc = new Intent(MainActivity.this, YourCustomers.class);
                startActivity(yc);
                break;
            case R.id.lacquittance:
                Intent la = new Intent(MainActivity.this, ListAcquittance.class);
                startActivity(la);
                break;
            case R.id.updateprofil:
                Intent up = new Intent(MainActivity.this, PVEdit.class);
                startActivity(up);
                break;
            case R.id.updateemailusername:
                Intent ueu = new Intent(MainActivity.this, EUEdit.class);
                startActivity(ueu);
                break;
            case R.id.updatepassword:
                Intent ups = new Intent(MainActivity.this, PWEdit.class);
                startActivity(ups);
                break;
            case R.id.logout:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Confirm!");
                alertDialog.setMessage("Are you sure to Log Out ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SaveSharedPreference.getInstance(getApplicationContext()).clear();
                                Intent logout = new Intent(MainActivity.this, VendorLogIn.class);
                                logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                Toast.makeText(MainActivity.this, "Log Out Success", Toast.LENGTH_SHORT).show();
                                startActivity(logout);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
            finish();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_view){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchProfilePicture() {
        Api api = Retroserver.getClient().create(Api.class);
        final String aid = SaveSharedPreference.getInstance(this).getId();
        Call<List<LogInVendorModel>> call = api.getProfilePicture(aid);
        call.enqueue(new Callback<List<LogInVendorModel>>() {
            @Override
            public void onResponse(Call<List<LogInVendorModel>> call, Response<List<LogInVendorModel>> response) {
                logInVendorModels = response.body();
                String pic = logInVendorModels.get(0).getPicture();
                byte[] decodeString = Base64.decode(pic, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                IMGPicture.setImageBitmap(bitmap);
            }

            @Override
            public void onFailure(Call<List<LogInVendorModel>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}
