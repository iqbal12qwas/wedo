package com.app.wedo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.LogInVendorModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VendorLogIn extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty
    private EditText username, password;

    private Button login, register;

    private CheckBox showhide;

    private Validator validator;

    boolean doubleBackToExitPressedOnce = false;

    private List<LogInVendorModel> logInVendorModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_log_in);
        setTitle("Log In Vendor");

        username = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password);

        login = (Button) findViewById(R.id.finish);
        register = (Button) findViewById(R.id.register);

        showhide = (CheckBox) findViewById(R.id.show_password);

        validator = new Validator(this);
        validator.setValidationListener(this);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(VendorLogIn.this, VendorRegister.class);
                startActivity(register);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
                if (username.getText().toString().equals("") && password.getText().toString().equals("") ) {
                    username.setError("Input Your Email / Username!");
                    password.setError("Input Your Password!");
                } else {
                    final Api loginapi = Retroserver.getClient().create(Api.class);
                    final String ausername = username.getText().toString();
                    final String apassword = password.getText().toString();

                    Call<List<LogInVendorModel>> login = loginapi.getVendor(ausername, apassword);
                    login.enqueue(new Callback<List<LogInVendorModel>>() {
                        @Override
                        public void onResponse(Call<List<LogInVendorModel>> call, Response<List<LogInVendorModel>> response) {
                            logInVendorModels = response.body();
                            SaveSharedPreference.getInstance(VendorLogIn.this).setLoggedIn(true,
                                    logInVendorModels.get(0).getId(),
                                    logInVendorModels.get(0).getEmail(),
                                    logInVendorModels.get(0).getUname(),
                                    apassword,
                                    logInVendorModels.get(0).getVname(),
                                    logInVendorModels.get(0).getNo_hp(),
                                    logInVendorModels.get(0).getProvinsi(),
                                    logInVendorModels.get(0).getVaddress(),
                                    logInVendorModels.get(0).getName(),
                                    logInVendorModels.get(0).getBirthday_date());
                            Toast.makeText(VendorLogIn.this, "Log In Succes", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(VendorLogIn.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }

                        @Override
                        public void onFailure(Call<List<LogInVendorModel>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Check Your Email/Username & Password", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        showhide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(SaveSharedPreference.getInstance(getApplicationContext()).getLoggedStatus()) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText)  {
                ((EditText) view).setError(message);
            } else if (view instanceof TextView) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
