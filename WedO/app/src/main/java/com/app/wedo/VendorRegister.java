package com.app.wedo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VendorRegister extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "VendorRegister";

    @NotEmpty
    private EditText email, username, name, password, repeat_password;

    private Button finish, login;

    private Validator validator;

    private CheckBox showhide;

    boolean doubleBackToExitPressedOnce = false;

    private Spinner provinsi;

    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_register);
        setTitle("Register Vendor");

        validator = new Validator(this);
        validator.setValidationListener(this);

        finish = (Button) findViewById(R.id.finish);
        login = (Button) findViewById(R.id.login);

        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.username);
        name = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password);
        repeat_password = (EditText) findViewById(R.id.repeat_password);

        showhide = (CheckBox) findViewById(R.id.show_password);

        final Spinner dropdownVendor = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.a_provinsi, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdownVendor.setAdapter(adapter);
        dropdownVendor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                adapterView.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(VendorRegister.this, VendorLogIn.class);
                startActivity(login);
                finish();
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
                if (email.getText().toString().length() == 0 && username.getText().toString().length() == 0 &&
                    name.getText().toString().length() == 0 && password.getText().toString().length() == 0 &&
                    repeat_password.getText().toString().length() == 0) {
                    email.setError("Please Input Here!");
                    username.setError("Please Input Here!");
                    name.setError("Please Input Here!");
                    password.setError("Please Input Here!");
                    repeat_password.setError("Please Input Here!");
                } else {
                    if (password.getText().toString().equals(repeat_password.getText().toString())){
                        final Api regisapi = Retroserver.getClient().create(Api.class);
                        final String aemail = email.getText().toString();
                        final String ausername = username.getText().toString();
                        final String aname = name.getText().toString();
                        final String apassword = password.getText().toString();
                        final String aprovinsi = dropdownVendor.getSelectedItem().toString();

                        retrofit2.Call<ResponModel> update = regisapi.register(aemail, ausername, aname, apassword, aprovinsi);
                        update.enqueue(new Callback<ResponModel>() {
                            @Override
                            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                String kode = response.body().getKode();
                                String pesan = response.body().getPesan();
                                if (pesan.equals("Email & Username Has Exist")){
                                    Toast.makeText(VendorRegister.this, pesan, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(VendorRegister.this, pesan, Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(VendorRegister.this, VendorLogIn.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponModel> call, Throwable t) {
                                Toast.makeText(VendorRegister.this, "Error", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Toast.makeText(VendorRegister.this, "Password & Repeat Password Not Same !", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        showhide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }



    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
