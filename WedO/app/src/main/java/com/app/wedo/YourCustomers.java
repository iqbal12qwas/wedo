package com.app.wedo;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.app.wedo.adapter.YourCustomerAdapter;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.YourCustomerModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YourCustomers extends AppCompatActivity {

    public static YourCustomers ma;
    private RecyclerView ycrecyclerView;
    private RecyclerView.LayoutManager yclayoutManager;
    private List<YourCustomerModel> yourCustomerModels;
    private YourCustomerAdapter ycadapter;
    private Api ycapi;
    ProgressBar ycprogressBar;

    SwipeRefreshLayout swipeRefreshLayout;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_customer);
        setTitle("Your Customers");

        id = SaveSharedPreference.getInstance(this).getId();

        ycprogressBar = findViewById(R.id.progress);
        ycrecyclerView = findViewById(R.id.YCrecyclerView);
        yclayoutManager = new LinearLayoutManager(this);
        ycrecyclerView.setLayoutManager(yclayoutManager);
        ycrecyclerView.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.yourcustomers_swipeup);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchYourCustomers(""); //without keyboard

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        ma = this;
        fetchYourCustomers(""); //without keyboard
    }

    public void fetchYourCustomers(String key) {
        ycapi = Retroserver.getClient().create(Api.class);
        final String id_vendor = id;
        Call<List<YourCustomerModel>> call = ycapi.getCustomer(id_vendor, key);
        call.enqueue(new Callback<List<YourCustomerModel>>() {
            @Override
            public void onResponse(Call<List<YourCustomerModel>> call, Response<List<YourCustomerModel>> response) {
                ycprogressBar.setVisibility(View.GONE);
                yourCustomerModels = response.body();
                ycadapter = new YourCustomerAdapter(yourCustomerModels, YourCustomers.this);
                ycrecyclerView.setAdapter(ycadapter);
                ycadapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<YourCustomerModel>> call, Throwable t) {
                ycprogressBar.setVisibility(View.GONE);
                Toast.makeText(YourCustomers.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        assert searchManager != null;
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchYourCustomers(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchYourCustomers(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(YourCustomers.this, MainActivity.class));
        finish();
    }
}
