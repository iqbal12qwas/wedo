package com.app.wedo.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.EditPackage.PTEdit;
import com.app.wedo.EditPackage.RTEdit;
import com.app.wedo.EditPackage.STEdit;
import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ListAcquittanceModel;
import com.app.wedo.model.ResponModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAcquittanceAdapter extends RecyclerView.Adapter<ListAcquittanceAdapter.MyViewHolder> {

    private List<ListAcquittanceModel> listAcquittanceModel;
    private Context context;

    private String id;

    public ListAcquittanceAdapter(List<ListAcquittanceModel> listAcquittanceModel, Context context) {
        this.listAcquittanceModel = listAcquittanceModel;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_acquittance, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.id.setText(listAcquittanceModel.get(position).getId());
        holder.name.setText(listAcquittanceModel.get(position).getName());
        holder.no_hp.setText(listAcquittanceModel.get(position).getNo_hp());
        holder.option.setText(listAcquittanceModel.get(position).getOption());
        holder.type.setText(listAcquittanceModel.get(position).getType());
        holder.order.setText(listAcquittanceModel.get(position).getOrder());
        holder.category.setText(listAcquittanceModel.get(position).getCategory());
        holder.booking.setText(listAcquittanceModel.get(position).getBooking());
        holder.price.setText(listAcquittanceModel.get(position).getPrice());
        holder.unit.setText(listAcquittanceModel.get(position).getUnit());
        holder.duration.setText(listAcquittanceModel.get(position).getDuration());
        holder.type_duration.setText(listAcquittanceModel.get(position).getType_duration());
        holder.note.setText(listAcquittanceModel.get(position).getNote());
        holder.paid.setText(listAcquittanceModel.get(position).getPaid());
        holder.minus.setText(listAcquittanceModel.get(position).getMinus());
        holder.total.setText(listAcquittanceModel.get(position).getTotal());
        holder.status.setText(listAcquittanceModel.get(position).getStatus());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                final String id2 = listAcquittanceModel.get(position).getId();
                final String id_product2 = listAcquittanceModel.get(position).getId_product();
                final String unit2 = listAcquittanceModel.get(position).getUnit();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Delete List Acquittance");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final Api laapi = Retroserver.getClient().create(Api.class);
                                laapi.deleteListAcquittance2(id2, id_product2, unit2).enqueue(new Callback<ResponModel>() {
                                    @Override
                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                        Toast.makeText(v.getContext(), "Delete Succes", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(v.getContext(), ListAcquittance.class);
                                        v.getContext().startActivity(intent);
                                        ((Activity) context).finish();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                        Toast.makeText(v.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
                return true;
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.type.getText().toString().equals("Rent")) {
                    Intent intent = new Intent(v.getContext(), RTEdit.class);
                    intent.putExtra("id", listAcquittanceModel.get(position).getId());
                    intent.putExtra("id_product", listAcquittanceModel.get(position).getId_product());
                    intent.putExtra("id_customer", listAcquittanceModel.get(position).getId_customer());
                    intent.putExtra("id_vendor", listAcquittanceModel.get(position).getId_vendor());
                    intent.putExtra("name", listAcquittanceModel.get(position).getName());
                    intent.putExtra("type", listAcquittanceModel.get(position).getType());
                    intent.putExtra("no_hp", listAcquittanceModel.get(position).getNo_hp());
                    intent.putExtra("option", listAcquittanceModel.get(position).getOption());
                    intent.putExtra("order", listAcquittanceModel.get(position).getOrder());
                    intent.putExtra("category", listAcquittanceModel.get(position).getCategory());
                    intent.putExtra("booking", listAcquittanceModel.get(position).getBooking());
                    intent.putExtra("price", listAcquittanceModel.get(position).getPrice());
                    intent.putExtra("unit", listAcquittanceModel.get(position).getUnit());
                    intent.putExtra("duration", listAcquittanceModel.get(position).getDuration());
                    intent.putExtra("type_duration", listAcquittanceModel.get(position).getType_duration());
                    intent.putExtra("note", listAcquittanceModel.get(position).getNote());
                    intent.putExtra("paid_before", listAcquittanceModel.get(position).getPaid_before());
                    intent.putExtra("paid", listAcquittanceModel.get(position).getPaid());
                    intent.putExtra("minus", listAcquittanceModel.get(position).getMinus());
                    intent.putExtra("total", listAcquittanceModel.get(position).getTotal());
                    intent.putExtra("status", listAcquittanceModel.get(position).getStatus());
                    intent.putExtra("pax", listAcquittanceModel.get(position).getPax());
                    intent.putExtra("total_pax", listAcquittanceModel.get(position).getTotal_pax());
                    v.getContext().startActivity(intent);
                } else if (holder.type.getText().toString().equals("Sell")){
                    Intent intent = new Intent(v.getContext(), STEdit.class);
                    intent.putExtra("id", listAcquittanceModel.get(position).getId());
                    intent.putExtra("id_product", listAcquittanceModel.get(position).getId_product());
                    intent.putExtra("id_customer", listAcquittanceModel.get(position).getId_customer());
                    intent.putExtra("id_vendor", listAcquittanceModel.get(position).getId_vendor());
                    intent.putExtra("name", listAcquittanceModel.get(position).getName());
                    intent.putExtra("no_hp", listAcquittanceModel.get(position).getNo_hp());
                    intent.putExtra("option", listAcquittanceModel.get(position).getOption());
                    intent.putExtra("type", listAcquittanceModel.get(position).getType());
                    intent.putExtra("order", listAcquittanceModel.get(position).getOrder());
                    intent.putExtra("category", listAcquittanceModel.get(position).getCategory());
                    intent.putExtra("booking", listAcquittanceModel.get(position).getBooking());
                    intent.putExtra("price", listAcquittanceModel.get(position).getPrice());
                    intent.putExtra("unit", listAcquittanceModel.get(position).getUnit());
                    intent.putExtra("duration", listAcquittanceModel.get(position).getDuration());
                    intent.putExtra("type_duration", listAcquittanceModel.get(position).getType_duration());
                    intent.putExtra("note", listAcquittanceModel.get(position).getNote());
                    intent.putExtra("paid_before", listAcquittanceModel.get(position).getPaid_before());
                    intent.putExtra("paid", listAcquittanceModel.get(position).getPaid());
                    intent.putExtra("minus", listAcquittanceModel.get(position).getMinus());
                    intent.putExtra("total", listAcquittanceModel.get(position).getTotal());
                    intent.putExtra("status", listAcquittanceModel.get(position).getStatus());
                    intent.putExtra("pax", listAcquittanceModel.get(position).getPax());
                    intent.putExtra("total_pax", listAcquittanceModel.get(position).getTotal_pax());
                    v.getContext().startActivity(intent);
                } else {
                    Intent intent = new Intent(v.getContext(), PTEdit.class);
                    intent.putExtra("id", listAcquittanceModel.get(position).getId());
                    intent.putExtra("id_product", listAcquittanceModel.get(position).getId_product());
                    intent.putExtra("id_customer", listAcquittanceModel.get(position).getId_customer());
                    intent.putExtra("id_vendor", listAcquittanceModel.get(position).getId_vendor());
                    intent.putExtra("name", listAcquittanceModel.get(position).getName());
                    intent.putExtra("no_hp", listAcquittanceModel.get(position).getNo_hp());
                    intent.putExtra("option", listAcquittanceModel.get(position).getOption());
                    intent.putExtra("type", listAcquittanceModel.get(position).getType());
                    intent.putExtra("order", listAcquittanceModel.get(position).getOrder());
                    intent.putExtra("category", listAcquittanceModel.get(position).getCategory());
                    intent.putExtra("booking", listAcquittanceModel.get(position).getBooking());
                    intent.putExtra("price", listAcquittanceModel.get(position).getPrice());
                    intent.putExtra("unit", listAcquittanceModel.get(position).getUnit());
                    intent.putExtra("duration", listAcquittanceModel.get(position).getDuration());
                    intent.putExtra("type_duration", listAcquittanceModel.get(position).getType_duration());
                    intent.putExtra("note", listAcquittanceModel.get(position).getNote());
                    intent.putExtra("paid_before", listAcquittanceModel.get(position).getPaid_before());
                    intent.putExtra("paid", listAcquittanceModel.get(position).getPaid());
                    intent.putExtra("minus", listAcquittanceModel.get(position).getMinus());
                    intent.putExtra("total", listAcquittanceModel.get(position).getTotal());
                    intent.putExtra("status", listAcquittanceModel.get(position).getStatus());
                    intent.putExtra("pax", listAcquittanceModel.get(position).getPax());
                    intent.putExtra("total_pax", listAcquittanceModel.get(position).getTotal_pax());
                    v.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listAcquittanceModel.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView id, name, no_hp, type, option, order, category, booking, price,
                unit, duration, type_duration, note, paid, minus, total, status;

        public MyViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.la_id);
            name = itemView.findViewById(R.id.la_name);
            no_hp = itemView.findViewById(R.id.msp_type);
            type = itemView.findViewById(R.id.la_type);
            option = itemView.findViewById(R.id.la_option);
            order = itemView.findViewById(R.id.la_order);
            category = itemView.findViewById(R.id.la_category);
            booking = itemView.findViewById(R.id.la_booking);
            price = itemView.findViewById(R.id.la_price);
            unit = itemView.findViewById(R.id.la_unit);
            duration = itemView.findViewById(R.id.la_drt);
            type_duration = itemView.findViewById(R.id.la_type_drt);
            note = itemView.findViewById(R.id.la_note);
            paid = itemView.findViewById(R.id.la_paid);
            minus = itemView.findViewById(R.id.la_minus);
            total = itemView.findViewById(R.id.la_total);
            status = itemView.findViewById(R.id.la_status);
        }
    }
}
