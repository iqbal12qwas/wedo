package com.app.wedo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.wedo.EditPackage.SPEdit;
import com.app.wedo.R;
import com.app.wedo.model.ListItemModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.MyViewHolder4> {

    private List<ListItemModel> listItemModels;
    private Context licontext;

    public ListItemAdapter(List<ListItemModel> listItemModels, Context licontext) {
        this.listItemModels = listItemModels;
        this.licontext = licontext;
    }

    @Override
    public MyViewHolder4 onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_service_or_product, parent, false);

        return new MyViewHolder4(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder4 holder, final int position) {
        Picasso.get().load("http://118.97.217.254/wedo/upload/product/" +
                listItemModels.get(position).getPicture()).fit().into(holder.picture);

        holder.option.setText(listItemModels.get(position).getOption());
        holder.type.setText(listItemModels.get(position).getType());
        holder.category.setText(listItemModels.get(position).getCategory());
        holder.name.setText(listItemModels.get(position).getName());
        holder.type_time.setText(listItemModels.get(position).getType_duration());
        holder.stock.setText(listItemModels.get(position).getStock());
        holder.price.setText("Rp. " + listItemModels.get(position).getPrice());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SPEdit.class);
                intent.putExtra("id", listItemModels.get(position).getId());
                intent.putExtra("id_vendor", listItemModels.get(position).getId_vendor());
                intent.putExtra("option", listItemModels.get(position).getOption());
                intent.putExtra("type", listItemModels.get(position).getType());
                intent.putExtra("category", listItemModels.get(position).getCategory());
                intent.putExtra("pax", listItemModels.get(position).getPax());
                intent.putExtra("category_package", listItemModels.get(position).getCategory_package());
                intent.putExtra("type_time", listItemModels.get(position).getType_duration());
                intent.putExtra("name", listItemModels.get(position).getName());
                intent.putExtra("price", listItemModels.get(position).getPrice());
                intent.putExtra("description", listItemModels.get(position).getDescription());
                intent.putExtra("stock", listItemModels.get(position).getStock());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItemModels.size();
    }

    public static class MyViewHolder4 extends RecyclerView.ViewHolder {

        TextView option, type, category, name, price, type_time, stock;
        ImageView picture;

        public MyViewHolder4(View itemView) {
            super(itemView);
            option = itemView.findViewById(R.id.msp_option);
            type = itemView.findViewById(R.id.msp_type);
            category = itemView.findViewById(R.id.msp_category);
            name = itemView.findViewById(R.id.msp_name);
            type_time = itemView.findViewById(R.id.msp_ttime);
            price = itemView.findViewById(R.id.msp_price);
            stock = itemView.findViewById(R.id.msp_stock);
            picture = (ImageView) itemView.findViewById(R.id.p_picture);
        }
    }


}
