package com.app.wedo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ListPaymentModel;
import com.app.wedo.model.ResponModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPaymentAdapter extends RecyclerView.Adapter<ListPaymentAdapter.MyViewHolder4> {

    private List<ListPaymentModel> listPaymentModels;
    private Context licontext;

    public ListPaymentAdapter(List<ListPaymentModel> listPaymentModels, Context licontext) {
        this.listPaymentModels = listPaymentModels;
        this.licontext = licontext;
    }

    @Override
    public MyViewHolder4 onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_payment, parent, false);

        return new MyViewHolder4(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder4 holder, final int position) {
        Picasso.get().load("http://118.97.217.254:8080/" +
                listPaymentModels.get(position).getPicture()).fit().rotate(90).into(holder.picture);

        holder.name.setText(listPaymentModels.get(position).getName());
        holder.date.setText(listPaymentModels.get(position).getDate());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                final String id = listPaymentModels.get(position).getId();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(licontext);
                alertDialog.setTitle("Delete This Proof of Payment");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final Api lpapi = Retroserver.getClient().create(Api.class);
                                lpapi.deleteProofofPayment(id).enqueue(new Callback<ResponModel>() {
                                    @Override
                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                        String pesan = response.body().getPesan();
                                        Toast.makeText(v.getContext(), pesan, Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(v.getContext(), ListAcquittance.class);
                                        v.getContext().startActivity(intent);
                                        ((Activity) licontext).finish();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                        Toast.makeText(v.getContext(), "Error", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPaymentModels.size();
    }

    public static class MyViewHolder4 extends RecyclerView.ViewHolder {

        TextView name;
        TextView date;
        ImageView picture;
        public MyViewHolder4(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.p_picture);
            name = itemView.findViewById(R.id.proof_name);
            date = itemView.findViewById(R.id.p_date);
        }

    }
}
