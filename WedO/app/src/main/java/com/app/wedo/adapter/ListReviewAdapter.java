package com.app.wedo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.wedo.R;
import com.app.wedo.model.ListReviewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListReviewAdapter extends RecyclerView.Adapter<ListReviewAdapter.MyViewHolder4>{

    private List<ListReviewModel> listReviewModel;
    private Context licontext;

    public ListReviewAdapter(List<ListReviewModel> listReviewModel, Context licontext) {
        this.listReviewModel = listReviewModel;
        this.licontext = licontext;
    }

    @Override
    public MyViewHolder4 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_review, parent, false);

        return new ListReviewAdapter.MyViewHolder4(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder4 holder, int position) {
        Picasso.get().load("http://118.97.217.254:8080/" +
                listReviewModel.get(position).getPicture()).fit().rotate(90).into(holder.picture);

        holder.time.setText(listReviewModel.get(position).getTime());
        holder.name.setText(listReviewModel.get(position).getName());
        holder.comment.setText(listReviewModel.get(position).getComment());
        holder.value.setText(listReviewModel.get(position).getRating());
        String rate = holder.value.getText().toString();
        final Float r = Float.parseFloat(rate);

        holder.rating.setRating(r);
    }

    @Override
    public int getItemCount() {
        return listReviewModel.size();
    }

    public static class MyViewHolder4 extends RecyclerView.ViewHolder {

        TextView name, comment, value, time;
        RatingBar rating;
        ImageView picture;

        public MyViewHolder4(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            comment = itemView.findViewById(R.id.comment);
            rating = itemView.findViewById(R.id.rating);
            picture = itemView.findViewById(R.id.picture);
            time = itemView.findViewById(R.id.tv_time_review);
            value = itemView.findViewById(R.id.value_review);
        }
    }
}
