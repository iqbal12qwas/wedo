package com.app.wedo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.EditPackage.LSEdit;
import com.app.wedo.ListSchedule;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ListScheduleModel;
import com.app.wedo.model.ResponModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListScheduleAdapter extends RecyclerView.Adapter<ListScheduleAdapter.MyViewHolder> {

    private List<ListScheduleModel> listScheduleModels;
    private Context lscontext;

    public ListScheduleAdapter(List<ListScheduleModel> listScheduleModels, Context lscontext) {
        this.listScheduleModels = listScheduleModels;
        this.lscontext = lscontext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_schedule, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.name.setText(listScheduleModels.get(position).getName());
        holder.no_hp.setText(listScheduleModels.get(position).getNo_hp());
        holder.event.setText(listScheduleModels.get(position).getEvent());
        holder.date.setText(listScheduleModels.get(position).getDate());
        holder.time.setText(listScheduleModels.get(position).getTime());
        holder.place.setText(listScheduleModels.get(position).getPlace());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LSEdit.class);
                intent.putExtra("id", listScheduleModels.get(position).getId());
                intent.putExtra("id_vendor", listScheduleModels.get(position).getId_vendor());
                intent.putExtra("name", listScheduleModels.get(position).getName());
                intent.putExtra("no_hp", listScheduleModels.get(position).getNo_hp());
                intent.putExtra("event", listScheduleModels.get(position).getEvent());
                intent.putExtra("date", listScheduleModels.get(position).getDate());
                intent.putExtra("time", listScheduleModels.get(position).getTime());
                intent.putExtra("place", listScheduleModels.get(position).getPlace());
                v.getContext().startActivity(intent);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                final String id = listScheduleModels.get(position).getId();
                Log.d("aaa", id);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(lscontext);
                alertDialog.setTitle("Delete This Schedule");
                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final Api lpapi = Retroserver.getClient().create(Api.class);
                                lpapi.deleteSchedule(id).enqueue(new Callback<ResponModel>() {
                                    @Override
                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                        String pesan = response.body().getPesan();
                                        Toast.makeText(v.getContext(), pesan, Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(v.getContext(), ListSchedule.class);
                                        v.getContext().startActivity(intent);
                                        ((Activity) lscontext).finish();
                                    }

                                    @Override
                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                        Toast.makeText(v.getContext(), "Error", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return listScheduleModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, no_hp, event, date, time, place;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ls_name);
            no_hp = itemView.findViewById(R.id.ls_no_hp);
            event = itemView.findViewById(R.id.ls_event);
            date = itemView.findViewById(R.id.ls_date);
            time = itemView.findViewById(R.id.ls_time);
            place = itemView.findViewById(R.id.ls_place);

        }
    }
}
