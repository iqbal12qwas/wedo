package com.app.wedo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.wedo.R;
import com.app.wedo.model.ListItemModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SPPictureAdapter extends RecyclerView.Adapter<SPPictureAdapter.MyViewHolder> {

    private List<ListItemModel> listItemModels;
    private Context context;

    public SPPictureAdapter(List<ListItemModel> listItemModels, Context context) {
        this.listItemModels = listItemModels;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_sp_sliding, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Picasso.get().load("http://118.97.217.254/wedo/upload/product/" +
                listItemModels.get(position).getPicture()).fit().into(holder.picture);
    }

    @Override
    public int getItemCount() {
        return listItemModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView picture;

        public MyViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.sp_picture_slide);
        }
    }
}
