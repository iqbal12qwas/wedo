package com.app.wedo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.wedo.EditPackage.YCEdit;
import com.app.wedo.R;
import com.app.wedo.model.YourCustomerModel;

import java.util.List;


public class YourCustomerAdapter extends RecyclerView.Adapter<YourCustomerAdapter.MyViewHolder2> {

    private List<YourCustomerModel> yourCustomerModels;
    private Context yccontext;

    public YourCustomerAdapter(List<YourCustomerModel> yourCustomerModels, Context context) {
        this.yourCustomerModels = yourCustomerModels;
        this.yccontext = context;
    }

    @Override
    public MyViewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_customer, parent, false);

        return new MyViewHolder2(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder2 holder, final int position) {
        final YourCustomerModel yourCustomerModel = yourCustomerModels.get(position);
        holder.tv_name.setText("Nama : " + yourCustomerModel.getName());
        holder.tv_no_hp.setText("No Hp : " + yourCustomerModel.getNo_hp());
        holder.tv_order.setText("Order : " + yourCustomerModel.getOrder());
        holder.tv_category.setText(yourCustomerModel.getCategory());
        holder.tv_date_order.setText(yourCustomerModel.getDate_order());
        holder.tv_type.setText(yourCustomerModel.getType());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), YCEdit.class);
                intent.putExtra("id", yourCustomerModels.get(position).getId());
                intent.putExtra("id_product", yourCustomerModels.get(position).getId_product());
                intent.putExtra("id_customer", yourCustomerModels.get(position).getId_customer());
                intent.putExtra("id_vendor", yourCustomerModels.get(position).getId_vendor());
                intent.putExtra("name", yourCustomerModels.get(position).getName());
                intent.putExtra("no_hp", yourCustomerModels.get(position).getNo_hp());
                intent.putExtra("order", yourCustomerModels.get(position).getOrder());
                intent.putExtra("category", yourCustomerModels.get(position).getCategory());
                intent.putExtra("type", yourCustomerModels.get(position).getType());
                intent.putExtra("option", yourCustomerModels.get(position).getOption());
                intent.putExtra("price", yourCustomerModels.get(position).getPrice());
                intent.putExtra("type_duration", yourCustomerModels.get(position).getType_duration());
                intent.putExtra("date_order",yourCustomerModels.get(position).getDate_order());
                intent.putExtra("status",yourCustomerModels.get(position).getStatus());
                intent.putExtra("pax",yourCustomerModels.get(position).getPax());
                intent.putExtra("stock",yourCustomerModels.get(position).getStock());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return yourCustomerModels.size();
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {

        TextView tv_name;
        TextView tv_no_hp;
        TextView tv_order;
        TextView tv_category;
        TextView tv_date_order;
        TextView tv_type;

        public MyViewHolder2(View itemView) {
            super(itemView);

            tv_name = (TextView) itemView.findViewById(R.id.ls_name);
            tv_no_hp = (TextView) itemView.findViewById(R.id.ls_no_hp);
            tv_order = (TextView) itemView.findViewById(R.id.cost_order);
            tv_category = (TextView) itemView.findViewById(R.id.cost_category);
            tv_date_order = (TextView) itemView.findViewById(R.id.ls_date);
            tv_type = (TextView) itemView.findViewById(R.id.cost_type);
        }
    }
}
