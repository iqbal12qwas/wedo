package com.app.wedo.api;

import com.app.wedo.model.ListAcquittanceModel;
import com.app.wedo.model.ListItemModel;
import com.app.wedo.model.ListPaymentModel;
import com.app.wedo.model.ListReviewModel;
import com.app.wedo.model.ListScheduleModel;
import com.app.wedo.model.LogInVendorModel;
import com.app.wedo.model.ResponModel;
import com.app.wedo.model.YourCustomerModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    //Schedule
    @FormUrlEncoded
    @POST("post/listSchedule.php")
    Call<ResponModel> inputSchedule(@Field("id_vendor") String id_vendor,
                                    @Field("name") String name,
                                    @Field("no_hp") String no_hp,
                                    @Field("event") String event,
                                    @Field("date") String date,
                                    @Field("time") String time,
                                    @Field("place") String place);

    @FormUrlEncoded
    @POST("path/updateSchedule.php")
    Call<ResponModel> updateSchedule(@Field("id") String id,
                                     @Field("name") String name,
                                     @Field("no_hp") String no_hp,
                                     @Field("event") String event,
                                     @Field("date") String date,
                                     @Field("time") String time,
                                     @Field("place") String place);

    @FormUrlEncoded
    @POST("delete/deletelistSchedule.php")
    Call<ResponModel> deleteSchedule(@Field("id") String id2);

    @FormUrlEncoded
    @POST("get/listSchedule.php")
    Call<List<ListScheduleModel>> getListSchedule(@Field("id_vendor") String id_vendor,
                                                  @Field("key") String keyword);



    //Sell Transaction
    @FormUrlEncoded
    @POST("post/sellTransaction.php")
    Call<ResponModel> sendSellTransaction(@Field("id_product") String id_product,
                                          @Field("id_customer") String id_customer,
                                          @Field("id_vendor") String id_vendor,
                                          @Field("name") String name,
                                          @Field("no_hp") String no_hp,
                                          @Field("order") String order,
                                          @Field("category") String category,
                                          @Field("option") String option,
                                          @Field("price") String price,
                                          @Field("type") String type,
                                          @Field("booking") String booking,
                                          @Field("unit") String unit,
                                          @Field("note") String note,
                                          @Field("paid") String paid,
                                          @Field("minus") String minus,
                                          @Field("total") String total,
                                          @Field("pax") String pax,
                                          @Field("stock") String stock);

    @FormUrlEncoded
    @POST("path/updatesellTransaction.php")
    Call<ResponModel> updateSellTransaction(@Field("id") String id,
                                            @Field("id_customer") String id_customer,
                                            @Field("id_product") String id_product,
                                            @Field("id_vendor") String id_vendor,
                                            @Field("name") String name,
                                            @Field("no_hp") String no_hp,
                                            @Field("order") String order,
                                            @Field("category") String category,
                                            @Field("option") String option,
                                            @Field("booking") String booking,
                                            @Field("price") String price,
                                            @Field("type") String type,
                                            @Field("unit") String unit,
                                            @Field("note") String note,
                                            @Field("paid_before") String paid_before,
                                            @Field("minus") String minus,
                                            @Field("total") String total,
                                            @Field("status") String status);

    @FormUrlEncoded
    @POST("post/dataSellTransaction.php")
    Call<ResponModel> sendDataST(@Field("id") String id,
                                 @Field("id_product") String id_product,
                                 @Field("id_customer") String id_customer,
                                 @Field("id_vendor") String id_vendor,
                                 @Field("name") String name,
                                 @Field("no_hp") String no_hp,
                                 @Field("type") String type,
                                 @Field("order") String order,
                                 @Field("category") String category,
                                 @Field("option") String option,
                                 @Field("booking") String booking,
                                 @Field("price") String price,
                                 @Field("unit") String unit,
                                 @Field("note") String note,
                                 @Field("paid") String paid,
                                 @Field("total") String total);


    //Rent Transaction
    @FormUrlEncoded
    @POST("path/updaterentTransaction.php")
    Call<ResponModel> updateRentTranssaction(@Field("id") String id,
                                             @Field("id_customer") String id_customer,
                                             @Field("id_product") String id_product,
                                             @Field("id_vendor") String id_vendor,
                                             @Field("name") String name,
                                             @Field("no_hp") String no_hp,
                                             @Field("order") String order,
                                             @Field("category") String category,
                                             @Field("option") String option,
                                             @Field("booking") String booking,
                                             @Field("price") String price,
                                             @Field("type") String type,
                                             @Field("duration") String duration,
                                             @Field("type_duration") String type_duration,
                                             @Field("note") String note,
                                             @Field("paid_before") String paid_before,
                                             @Field("minus") String minus,
                                             @Field("total") String total,
                                             @Field("status") String status);

    @FormUrlEncoded
    @POST("post/rentTransaction.php")
    Call<ResponModel> sendRentTranssaction(@Field("id_product") String id_product,
                                           @Field("id_customer") String id_customer,
                                           @Field("id_vendor") String id_vendor,
                                           @Field("name") String name,
                                           @Field("no_hp") String no_hp,
                                           @Field("order") String order,
                                           @Field("category") String category,
                                           @Field("option") String option,
                                           @Field("price") String price,
                                           @Field("type") String type,
                                           @Field("booking") String booking,
                                           @Field("duration") String duration,
                                           @Field("type_duration") String type_duration,
                                           @Field("note") String note,
                                           @Field("paid") String paid,
                                           @Field("minus") String minus,
                                           @Field("total") String total,
                                           @Field("pax") String pax,
                                           @Field("total_pax") String total_pax);

    @FormUrlEncoded
    @POST("post/dataRentTransaction.php")
    Call<ResponModel> sendDataRT(@Field("id") String id,
                                 @Field("id_product") String id_product,
                                 @Field("id_customer") String id_customer,
                                 @Field("id_vendor") String id_vendor,
                                 @Field("name") String name,
                                 @Field("no_hp") String no_hp,
                                 @Field("order") String order,
                                 @Field("category") String category,
                                 @Field("option") String option,
                                 @Field("price") String price,
                                 @Field("type") String type,
                                 @Field("booking") String booking,
                                 @Field("duration") String duration,
                                 @Field("type_duration") String type_duration,
                                 @Field("note") String note,
                                 @Field("paid") String paid,
                                 @Field("total") String total);

    //Package Transaction
    @FormUrlEncoded
    @POST("post/packageTransaction.php")
    Call<ResponModel> sendPackageTransaction(@Field("id_product") String id_product,
                                             @Field("id_customer") String id_customer,
                                             @Field("id_vendor") String id_vendor,
                                             @Field("name") String name,
                                             @Field("no_hp") String no_hp,
                                             @Field("order") String order,
                                             @Field("category") String category,
                                             @Field("option") String option,
                                             @Field("price") String price,
                                             @Field("type") String type,
                                             @Field("booking") String booking,
                                             @Field("note") String note,
                                             @Field("paid") String paid,
                                             @Field("minus") String minus,
                                             @Field("total") String total,
                                             @Field("pax") String pax);

    @FormUrlEncoded
    @POST("post/dataPackageTransaction.php")
    Call<ResponModel> sendDataPT(@Field("id") String id,
                                 @Field("id_product") String id_product,
                                 @Field("id_customer") String id_customer,
                                 @Field("id_vendor") String id_vendor,
                                 @Field("name") String name,
                                 @Field("no_hp") String no_hp,
                                 @Field("order") String order,
                                 @Field("category") String category,
                                 @Field("option") String option,
                                 @Field("price") String price,
                                 @Field("type") String type,
                                 @Field("booking") String booking,
                                 @Field("note") String note,
                                 @Field("paid") String paid,
                                 @Field("total") String total);


    @FormUrlEncoded
    @POST("path/updatepackageTransaction.php")
    Call<ResponModel> updatePackageTranssaction(@Field("id") String id,
                                                @Field("id_customer") String id_customer,
                                                @Field("id_product") String id_product,
                                                @Field("id_vendor") String id_vendor,
                                                @Field("name") String name,
                                                @Field("no_hp") String no_hp,
                                                @Field("order") String order,
                                                @Field("category") String category,
                                                @Field("option") String option,
                                                @Field("booking") String booking,
                                                @Field("price") String price,
                                                @Field("type") String type,
                                                @Field("note") String note,
                                                @Field("paid_before") String paid_before,
                                                @Field("minus") String minus,
                                                @Field("total") String total,
                                                @Field("status") String status);



    //Acquittance
    @FormUrlEncoded
    @POST("delete/deletelistAcquittance.php")
    Call<ResponModel> deleteListAcquittance(@Field("id") String id,
                                            @Field("id_product") String id_product);

    @FormUrlEncoded
    @POST("delete/deletelistAcquittance2.php")
    Call<ResponModel> deleteListAcquittance2(@Field("id") String id,
                                             @Field("id_product") String id_product,
                                             @Field("unit") String unit);

    @FormUrlEncoded
    @POST("get/listAcquittance.php")
    Call<List<ListAcquittanceModel>> getListAcquittance(@Field("id_vendor") String id_vendor,
                                                        @Field("key") String keyword);

    @FormUrlEncoded
    @POST("get/listProofOfPayment.php")
    Call<List<ListPaymentModel>> getProofOfPayment(@Field("id_order") String id_order,
                                                   @Field("name") String name,
                                                   @Field("key") String keyword);

    @FormUrlEncoded
    @POST("delete/deleteProofOfPayment.php")
    Call<ResponModel> deleteProofofPayment(@Field("id") String id);


    //Your Customer
    @FormUrlEncoded
    @POST("delete/deleteyourCustomer.php")
    Call<ResponModel> deleteYourCustomer(@Field("id") String id);

    @FormUrlEncoded
    @POST("get/yourCustomer.php")
    Call<List<YourCustomerModel>> getCustomer(@Field("id_vendor") String id_vendor,
                                              @Field("key") String keyword);


    //List
    @FormUrlEncoded
    @POST("get/product.php")
    Call<List<ListItemModel>> getItem(@Field("id_vendor") String id_vendor,
                                      @Field("key") String keyword);

    @GET("get/slidePicture.php")
    Call<List<ListItemModel>> getSPPictureModel(@Query("id") String id,
                                                @Query("id_vendor") String id_vendor);

    @FormUrlEncoded
    @POST("post/review.php")
    Call<List<ListReviewModel>> getReview(@Field("id_product") String id_product,
                                          @Field("id_vendor") String id_vendor);


    //User Control
    @FormUrlEncoded
    @POST("post/register.php")
    Call<ResponModel> register(@Field("email") String email,
                               @Field("uname") String uname,
                               @Field("name") String name,
                               @Field("password") String password,
                               @Field("provinsi") String provinsi);


    @FormUrlEncoded
    @POST("post/login.php")
    Call<List<LogInVendorModel>> getVendor(@Field("uname") String uname,
                                           @Field("password") String password);

    @FormUrlEncoded
    @POST("user/login")
    Call<List<LogInVendorModel>> getVendor2(@Field("email") String email,
                                            @Field("password") String password);

    @FormUrlEncoded
    @POST("user/signup")
    Call<ResponModel> register2(@Field("email") String email,
                                @Field("password") String password,
                                @Field("role") String role);

    @FormUrlEncoded
    @POST("path/updateprofilm.php")
    Call<ResponModel> updateprofil(@Field("id") String id,
                                   @Field("name") String name,
                                   @Field("birthday") String birthday,
                                   @Field("vendorname") String vendorname,
                                   @Field("no_hp") String no_hp,
                                   @Field("vaddress") String vaddress,
                                   @Field("provinsi") String provinsi);


    @FormUrlEncoded
    @POST("path/updatephotoprofilem.php")
    Call<ResponModel> updatephotoprofil(@Field("id") String id,
                                        @Field("picture") String picture);


    @FormUrlEncoded
    @POST("path/updateemailusernamem.php")
    Call<ResponModel> updateeuname(@Field("id") String id,
                                   @Field("email") String email,
                                   @Field("uname") String uname);

    @FormUrlEncoded
    @POST("get/getRating.php")
    Call<ResponModel> getrating(@Field("id_product") String id_product,
                                @Field("id_vendor") String id_vendor);

    @FormUrlEncoded
    @POST("path/updatepasswordm.php")
    Call<ResponModel> updatepassword(@Field("id") String id,
                                     @Field("password") String password);

    @FormUrlEncoded
    @POST("get/pictureprofile.php")
    Call<List<LogInVendorModel>> getProfilePicture(@Field("id") String id);
}