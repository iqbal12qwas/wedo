package com.app.wedo.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retroserver {
    public static final String base_url = "http://118.97.217.254/wedo/api/";

    public static Retrofit retrofit;

    public  static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(200,TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url).client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
