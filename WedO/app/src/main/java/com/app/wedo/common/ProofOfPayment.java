package com.app.wedo.common;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.adapter.ListPaymentAdapter;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.model.ListPaymentModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProofOfPayment extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ListPaymentModel> listPaymentModels;
    private ListPaymentAdapter adapter;
    private Api api;
    ProgressBar progressBar;

    SwipeRefreshLayout swipeRefreshLayout;

    private String id, id_order, id_customer, id_product, id_vendor, name, no_hp, order, category, type, option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proof_of_payment);
        setTitle("Proof of Payment");

        id = SaveSharedPreference.getInstance(this).getId();

        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.PoPPrecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.proofofpayment_swipeup);

        Intent intent = getIntent();
        id_order = intent.getStringExtra("id");
        Log.d("aaa", id_order);
        id_customer = intent.getStringExtra("id_customer");
        id_product = intent.getStringExtra("id_product");
        id_vendor = intent.getStringExtra("id_vendor");
        name = intent.getStringExtra("name");
        no_hp = intent.getStringExtra("no_hp");
        order = intent.getStringExtra("order");
        category = intent.getStringExtra("category");
        type = intent.getStringExtra("type");
        option = intent.getStringExtra("option");

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchProofOfPayment(""); //without keyboard

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        fetchProofOfPayment(""); //without keyboard
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        assert searchManager != null;
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchProofOfPayment(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchProofOfPayment(newText);
                return false;
            }
        });
        return true;
    }

    private void fetchProofOfPayment(String key) {
        api = Retroserver.getClient().create(Api.class);
        final String aid_order = id_order;
        final String aname = name;
        Call<List<ListPaymentModel>> call = api.getProofOfPayment(aid_order, aname, key);
        call.enqueue(new Callback<List<ListPaymentModel>>() {
            @Override
            public void onResponse(Call<List<ListPaymentModel>> call, Response<List<ListPaymentModel>> response) {
                progressBar.setVisibility(View.GONE);
                listPaymentModels = response.body();
                adapter = new ListPaymentAdapter(listPaymentModels, ProofOfPayment.this);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ListPaymentModel>> call, Throwable t) {
                Toast.makeText(ProofOfPayment.this, "Error on :" + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ProofOfPayment.this, ListAcquittance.class));
        finish();
    }
}
