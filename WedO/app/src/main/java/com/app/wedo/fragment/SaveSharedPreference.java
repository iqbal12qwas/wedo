package com.app.wedo.fragment;

import android.content.Context;
import android.content.SharedPreferences;


public class SaveSharedPreference {
    private static final String SHARED_PREF_NAME = "authorization_token";
    public static final String LOGGED_IN_PREF = "logged_in_status";
    public static final String KEY_USERNAME = "uname";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_VENDOR_NAME = "vname";
    public static final String KEY_NO_HP = "no_hp";
    public static final String KEY_PROVINSI = "provinsi";
    public static final String KEY_VENDOR_ADDRESS = "vaddress";
    public static final String KEY_BIRTHDAY_DATE = "birthday_date";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAME = "name";
    public static final String KEY_USER_ID = "id";

    public static final String KEY_CODE = "kode";
    public static final String KEY_MESSAGE = "pesan";


    private static SaveSharedPreference mInstance;
    private Context mCtx;

    private SaveSharedPreference(Context mCtx) {
        this.mCtx = mCtx;
    }


    public static synchronized SaveSharedPreference getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new SaveSharedPreference(mCtx);
        }
        return mInstance;
    }

    public void setLoggedIn(boolean loggedIn, String id, String email, String uname, String password, String vname, String no_hp, String provinsi, String vaddress, String name, String birthday_date ) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.putString(KEY_USER_ID, id);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USERNAME, uname);
        editor.putString(KEY_PASSWORD, password);
        editor.putString(KEY_VENDOR_NAME, vname);
        editor.putString(KEY_NO_HP, no_hp);
        editor.putString(KEY_PROVINSI, provinsi);
        editor.putString(KEY_VENDOR_ADDRESS, vaddress);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_BIRTHDAY_DATE, birthday_date);


        editor.apply();
    }

    public void setUpdateEmailUsername(String email, String uname) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USERNAME, uname);


        editor.apply();
    }

    public void setUpdateProfile(String name, String birthday, String vname, String no_hp, String vaddress, String provinsi) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_NAME, name);
        editor.putString(KEY_BIRTHDAY_DATE, birthday);
        editor.putString(KEY_VENDOR_NAME, vname);
        editor.putString(KEY_NO_HP, no_hp);
        editor.putString(KEY_VENDOR_ADDRESS, vaddress);
        editor.putString(KEY_PROVINSI, provinsi);

        editor.apply();
    }


    public void saveToken(String token) {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("token", token);

        editor.apply();

    }

    public boolean getLoggedStatus() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(LOGGED_IN_PREF, false);
    }

    public String getId() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_ID, null);
    }

    public String getEmail() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_EMAIL, null);
    }

    public String getUsername() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null);
    }

    public String getPassword() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PASSWORD, null);
    }

    public String getVendorName() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_VENDOR_NAME, null);
    }

    public String getNoHp() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NO_HP, null);
    }

    public String getProvinsi() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PROVINSI, null);
    }

    public String getVendorAddress() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_VENDOR_ADDRESS, null);
    }

    public String getName() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NAME, null);
    }

    public String getBirthdayDate() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_BIRTHDAY_DATE, null);
    }

    public String getToken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("token", null);
    }

    public void clear() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
