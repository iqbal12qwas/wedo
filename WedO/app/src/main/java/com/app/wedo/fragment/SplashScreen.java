package com.app.wedo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.app.wedo.R;
import com.app.wedo.VendorLogIn;

public class SplashScreen extends AppCompatActivity {

    private ImageView logo;
    private static int splashTimeOut = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        logo = (ImageView) findViewById(R.id.splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, VendorLogIn.class);
                startActivity(i);
                finish();
            }
        },splashTimeOut);

        Animation animation = AnimationUtils.loadAnimation(this,R.anim.splashscreen);
        logo.startAnimation(animation);
    }
}
