package com.app.wedo.model;

public class ListAcquittanceModel {
    private String id;
    private String id_product;
    private String id_customer;
    private String id_vendor;
    private String name;
    private String no_hp;
    private String option;
    private String type;
    private String order;
    private String category;
    private String date_transaction;
    private String booking;
    private String price;
    private String unit;
    private String duration;
    private String type_duration;
    private String note;
    private String paid_before;
    private String paid;
    private String minus;
    private String total;
    private String status;
    private String pax;
    private String total_pax;

    //constructor
    public ListAcquittanceModel(String id, String id_product, String id_customer, String id_vendor, String name, String no_hp, String option, String type, String order, String category, String date_transaction, String booking, String price, String unit, String duration, String type_duration, String note, String paid_before, String paid, String minus, String total, String status, String pax, String total_pax) {
        this.id = id;
        this.id_product = id_product;
        this.id_customer = id_customer;
        this.id_vendor = id_vendor;
        this.name = name;
        this.no_hp = no_hp;
        this.option = option;
        this.type = type;
        this.order = order;
        this.category = category;
        this.date_transaction = date_transaction;
        this.booking = booking;
        this.price = price;
        this.unit = unit;
        this.duration = duration;
        this.type_duration = type_duration;
        this.note = note;
        this.paid_before = paid_before;
        this.paid = paid;
        this.minus = minus;
        this.total = total;
        this.status = status;
        this.pax = pax;
        this.total_pax = total_pax;
    }

    //Getter
    public String getId() {
        return id;
    }

    public String getId_product() {
        return id_product;
    }

    public String getId_customer() {
        return id_customer;
    }

    public String getId_vendor() {
        return id_vendor;
    }

    public String getName() {
        return name;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public String getOption() {
        return option;
    }

    public String getType() {
        return type;
    }

    public String getOrder() {
        return order;
    }

    public String getCategory() {
        return category;
    }

    public String getDate_transaction() {
        return date_transaction;
    }

    public String getBooking() {
        return booking;
    }

    public String getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getDuration() {
        return duration;
    }

    public String getType_duration() {
        return type_duration;
    }

    public String getNote() {
        return note;
    }

    public String getPaid_before() {
        return paid_before;
    }

    public String getPaid() {
        return paid;
    }

    public String getMinus() {
        return minus;
    }

    public String getTotal() {
        return total;
    }

    public String getStatus() {
        return status;
    }

    public String getPax() {
        return pax;
    }

    public String getTotal_pax() {
        return total_pax;
    }
}
