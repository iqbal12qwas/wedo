package com.app.wedo.model;

public class ListItemModel {
    private String id;
    private String id_vendor;
    private String picture;
    private String option;
    private String type;
    private String category;
    private String pax;
    private String category_package;
    private String type_duration;
    private String name;
    private String description;
    private String price;
    private String stock;


    //constructor
    public ListItemModel(String id, String id_vendor, String picture, String option, String type, String category, String pax, String category_package, String type_duration, String name, String description, String price, String stock) {
        this.id = id;
        this.id_vendor = id_vendor;
        this.picture = picture;
        this.option = option;
        this.type = type;
        this.category = category;
        this.pax = pax;
        this.category_package = category_package;
        this.type_duration = type_duration;
        this.name = name;
        this.description = description;
        this.price = price;
        this.stock = stock;
    }

    //getters
    public String getId() {
        return id;
    }

    public String getId_vendor() {
        return id_vendor;
    }

    public String getPicture() {
        return picture;
    }

    public String getOption() {
        return option;
    }

    public String getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    public String getPax() {
        return pax;
    }

    public String getCategory_package() {
        return category_package;
    }

    public String getType_duration() {
        return type_duration;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getStock() {
        return stock;
    }
}
