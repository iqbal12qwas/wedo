package com.app.wedo.model;

public class ListPaymentModel {
    private String id;
    private String id_order;
    private String name;
    private String date;
    private String picture;


    //Constructor
    public ListPaymentModel(String id, String id_order, String name, String date, String picture) {
        this.id = id;
        this.id_order = id_order;
        this.name = name;
        this.date = date;
        this.picture = picture;
    }

    //Getter
    public String getId() {
        return id;
    }

    public String getId_order() {
        return id_order;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getPicture() {
        return picture;
    }
}
