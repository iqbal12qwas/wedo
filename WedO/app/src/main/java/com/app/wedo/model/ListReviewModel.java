package com.app.wedo.model;

public class ListReviewModel {

    private String id;
    private String id_product;
    private String id_customer;
    private String id_vendor;
    private String picture;
    private String email;
    private String name;
    private String comment;
    private String rating;
    private String time;


    //Constructor
    public ListReviewModel(String id, String id_product, String id_customer, String id_vendor, String picture, String email, String name, String comment, String rating, String time) {
        this.id = id;
        this.id_product = id_product;
        this.id_customer = id_customer;
        this.id_vendor = id_vendor;
        this.picture = picture;
        this.email = email;
        this.name = name;
        this.comment = comment;
        this.rating = rating;
        this.time = time;
    }

    //Getter
    public String getId() {
        return id;
    }

    public String getId_product() {
        return id_product;
    }

    public String getId_customer() {
        return id_customer;
    }

    public String getId_vendor() {
        return id_vendor;
    }

    public String getPicture() {
        return picture;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getRating() {
        return rating;
    }

    public String getTime() {
        return time;
    }
}
