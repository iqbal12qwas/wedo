package com.app.wedo.model;

public class ListScheduleModel {

    String id;
    String id_vendor;
    String event;
    String name;
    String no_hp;
    String place;
    String date;
    String time;

    //constructor
    public ListScheduleModel(String id, String id_vendor, String event, String name, String no_hp, String place, String date, String time) {
        this.id = id;
        this.id_vendor = id_vendor;
        this.event = event;
        this.name = name;
        this.no_hp = no_hp;
        this.place = place;
        this.date = date;
        this.time = time;
    }

    //getter
    public String getId() {
        return id;
    }

    public String getId_vendor() {
        return id_vendor;
    }

    public String getEvent() {
        return event;
    }

    public String getName() {
        return name;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public String getPlace() {
        return place;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }
}
