package com.app.wedo.model;

public class LogInVendorModel {

    String id;
    String picture;
    String email;
    String uname;
    String password;
    String vname;
    String no_hp;
    String provinsi;
    String vaddress;
    String name;
    String birthday_date;

    //Constructor
    public LogInVendorModel(String id, String picture, String email, String uname, String password, String vname, String no_hp, String provinsi, String vaddress, String name, String birthday_date) {
        this.id = id;
        this.picture = picture;
        this.email = email;
        this.uname = uname;
        this.password = password;
        this.vname = vname;
        this.no_hp = no_hp;
        this.provinsi = provinsi;
        this.vaddress = vaddress;
        this.name = name;
        this.birthday_date = birthday_date;
    }


    //Getter
    public String getId() {
        return id;
    }

    public String getPicture() {
        return picture;
    }

    public String getEmail() {
        return email;
    }

    public String getUname() {
        return uname;
    }

    public String getPassword() {
        return password;
    }

    public String getVname() {
        return vname;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public String getVaddress() {
        return vaddress;
    }

    public String getName() {
        return name;
    }

    public String getBirthday_date() {
        return birthday_date;
    }
}
