package com.app.wedo.model;

public class ResponModel {

    String kode;
    String pesan;

    String picture;

    public ResponModel(String kode, String pesan) {
        this.kode = kode;
        this.pesan = pesan;
    }

    public ResponModel(String picture) {
        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getKode(){
        return kode;
    }

    public void setKode(String kode){
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
