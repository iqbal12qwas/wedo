package com.app.wedo.model;

public class SPEditModel {

    private String picture;

    public SPEditModel(String picture) {
        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }
}
