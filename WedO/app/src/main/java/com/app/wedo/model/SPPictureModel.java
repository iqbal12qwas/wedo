package com.app.wedo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SPPictureModel {
    @SerializedName("listitemmodelss")
    private ArrayList<ListItemModel> listItemModels;

    public SPPictureModel(){

    }

    public ArrayList<ListItemModel> getItem(){
        return listItemModels;
    }
}
