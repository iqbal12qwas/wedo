package com.app.wedo.model;

public class YourCustomerModel {
    private String id;
    private String id_product;
    private String id_customer;
    private String id_vendor;
    private String name;
    private String no_hp;
    private String order;
    private String category;
    private String price;
    private String option;
    private String type;
    private String type_duration;
    private String date_order;
    private String status;
    private String pax;
    private String stock;

    //constructor
    public YourCustomerModel(String id, String id_product, String id_customer, String id_vendor, String name, String no_hp, String order, String category, String price, String option, String type, String type_duration, String date_order, String status, String pax, String stock) {
        this.id = id;
        this.id_product = id_product;
        this.id_customer = id_customer;
        this.id_vendor = id_vendor;
        this.name = name;
        this.no_hp = no_hp;
        this.order = order;
        this.category = category;
        this.price = price;
        this.option = option;
        this.type = type;
        this.type_duration = type_duration;
        this.date_order = date_order;
        this.status = status;
        this.pax = pax;
        this.stock = stock;
    }

    //getters
    public String getId() {
        return id;
    }

    public String getId_product() {
        return id_product;
    }

    public String getId_customer() {
        return id_customer;
    }

    public String getId_vendor() {
        return id_vendor;
    }

    public String getName() {
        return name;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public String getOrder() {
        return order;
    }

    public String getCategory() {
        return category;
    }

    public String getPrice() {
        return price;
    }

    public String getOption() {
        return option;
    }

    public String getType() {
        return type;
    }

    public String getType_duration() {
        return type_duration;
    }

    public String getDate_order() {
        return date_order;
    }

    public String getStatus() {
        return status;
    }

    public String getPax() {
        return pax;
    }

    public String getStock() {
        return stock;
    }
}

