package com.app.wedo.popup;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.wedo.ListSchedule;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.fragment.SaveSharedPreference;
import com.app.wedo.fragment.TimePickerFragment;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class AddListSchedule extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, Validator.ValidationListener {

    private static final String TAG = "AddListSchedule";

    @NotEmpty
    private EditText name, no_hp, event, place;

    private Button done;

    private TextView time, date;

    private DatePickerDialog.OnDateSetListener DateListener;

    private Validator validator;

    final Context context = this;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_list_schedule);
        setTitle("Input Schedule");

        id = SaveSharedPreference.getInstance(this).getId();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .8));

        name = (EditText) findViewById(R.id.ls_name);
        no_hp = (EditText) findViewById(R.id.ls_no_hp);
        event = (EditText) findViewById(R.id.ls_event);
        place = (EditText) findViewById(R.id.ls_place);
        date = (TextView) findViewById(R.id.ls_date);
        time = (TextView) findViewById(R.id.ls_time);

        validator = new Validator(this);
        validator.setValidationListener(this);

        done = (Button) findViewById(R.id.btn_ls_done);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "Time Picker");
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        AddListSchedule.this, android.R.style.Theme_Holo_Dialog_MinWidth,
                        DateListener,
                        year, month, day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        DateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String datee = year + "/" + month + "/" + day;
                date.setText(datee);
            }
        };

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
                final String aa_date = date.getText().toString();
                final String aa_time = time.getText().toString();
                if ((name.getText().toString().length() == 0) && (no_hp.getText().toString().length() == 0) &&
                        (event.getText().toString().length() == 0) && (place.getText().toString().length() == 0) &&
                        (aa_date.equals("YYYY/MM/DD")) && (aa_time.equals("HH:MM:SS"))) {
                    Toast.makeText(AddListSchedule.this, "Please Set Date & Time", Toast.LENGTH_LONG).show();
                    date.setError("Please Set Your Date!");
                    time.setError("Please Set Your Time!");
                    name.setError("Please Input Here!");
                    no_hp.setError("Please Input Here!");
                    event.setError("Please Input Here!");
                    place.setError("Please Input Here!");
                } else {
                    if (aa_date.equals("YYYY/MM/DD")) {
                        Toast.makeText(AddListSchedule.this, "Please Set Schedule Date", Toast.LENGTH_LONG).show();
                    } else {
                        if (aa_time.equals("HH:MM:SS")) {
                            Toast.makeText(AddListSchedule.this, "Please Set Schedule Time", Toast.LENGTH_LONG).show();
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle("Add Your Schedule");
                            alertDialog.setMessage("Are you sure ?").setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            final ProgressDialog progressDoalog;
                                            progressDoalog = new ProgressDialog(AddListSchedule.this);
                                            progressDoalog.setMessage("Loading....");
                                            progressDoalog.setTitle("Progress");
                                            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                                            // show it
                                            progressDoalog.show();
                                            progressDoalog.setCancelable(false);
                                            progressDoalog.setCanceledOnTouchOutside(false);
                                            final Api lsapi = Retroserver.getClient().create(Api.class);
                                            final String id_vendor = id;
                                            final String a_name = name.getText().toString();
                                            final String a_no_hp = no_hp.getText().toString();
                                            final String a_event = event.getText().toString();
                                            final String a_date = date.getText().toString();
                                            final String a_time = time.getText().toString();
                                            final String a_place = place.getText().toString();

                                            Call<ResponModel> update = lsapi.inputSchedule(id_vendor, a_name, a_no_hp, a_event, a_date, a_time, a_place);
                                            update.enqueue(new Callback<ResponModel>() {
                                                @Override
                                                public void onResponse(Call<ResponModel> call, retrofit2.Response<ResponModel> response) {
                                                    progressDoalog.dismiss();
                                                    startActivity(new Intent(AddListSchedule.this, ListSchedule.class)
                                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                    Toast.makeText(getApplicationContext(), "Successfully", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }

                                                @Override
                                                public void onFailure(Call<ResponModel> call, Throwable t) {
                                                    Toast.makeText(AddListSchedule.this, "Error", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog1 = alertDialog.create();
                            alertDialog.show();
                        }
                    }
                }
            }
        });

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        TextView textView = (TextView) findViewById(R.id.ls_time);
        textView.setText(hourOfDay + ":" + minute + ":00");
    }

    @Override
    public void onValidationSucceeded() {
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else if (view instanceof TextView) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
