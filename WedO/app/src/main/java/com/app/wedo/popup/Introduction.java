package com.app.wedo.popup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.app.wedo.R;
import com.app.wedo.VendorLogIn;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class Introduction extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_introduction);

        addSlide(AppIntroFragment.newInstance("Item & Package","Only display your Item or Package",
                R.drawable.itempackage, ContextCompat.getColor(getApplicationContext(),R.color.BackgroundIntro)));
        addSlide(AppIntroFragment.newInstance("Your Customer","Customer has Booked your Item or Package",
                R.drawable.customer, ContextCompat.getColor(getApplicationContext(),R.color.BackgroundIntro)));
        addSlide(AppIntroFragment.newInstance("Payment","This Payment have 3 methods : Package, Rent, and Sell",
                R.drawable.payment, ContextCompat.getColor(getApplicationContext(),R.color.BackgroundIntro)));
        addSlide(AppIntroFragment.newInstance("Proof Of Payment","Customer can send Proof of Payment by Picture",
                R.drawable.ppf, ContextCompat.getColor(getApplicationContext(),R.color.BackgroundIntro)));
        addSlide(AppIntroFragment.newInstance("Schedule","You can add Schedule with your Team or Customer",
                R.drawable.schedule, ContextCompat.getColor(getApplicationContext(),R.color.BackgroundIntro)));
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent=new Intent(getApplicationContext(), VendorLogIn.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent=new Intent(getApplicationContext(), VendorLogIn.class);
        startActivity(intent);
        finish();
    }

}
