package com.app.wedo.transaction;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PackageTransaction extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "PackageTransaction";

    ImageButton ptAddition, ptMinus;

    @NotEmpty
    private EditText ptPaid;

    private Button ptFinish;

    final Context context = this;

    private EditText ptNote, ptABudget, ptMBudget;
    private TextView ptDate, ptResidue, ptType, ptName, ptNoHp, ptOrder, ptCategory, ptPrice, ptTotal, ptOption, ptPtABudget, ptPtMBudget;
    private DatePickerDialog.OnDateSetListener ptDateListener;

    private Validator validator;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_transaction);
        setTitle("Package Transaction");

        ptAddition = (ImageButton) findViewById(R.id.addition);
        ptMinus = (ImageButton) findViewById(R.id.minus);

        ptPtMBudget = (TextView) findViewById(R.id.pt_pt_mb);
        ptPtABudget = (TextView) findViewById(R.id.pt_pt_ab);
        ptDate = (TextView) findViewById(R.id.pt_date);
        ptResidue = (TextView) findViewById(R.id.pt_residue);
        ptName = (TextView) findViewById(R.id.pt_name);
        ptNoHp = (TextView) findViewById(R.id.pt_no_hp);
        ptOrder = (TextView) findViewById(R.id.pt_order);
        ptCategory = (TextView) findViewById(R.id.pt_category);
        ptOption = (TextView) findViewById(R.id.pt_option);
        ptPrice = (TextView) findViewById(R.id.pt_price);
        ptTotal = (TextView) findViewById(R.id.pt_total);
        ptType = (TextView) findViewById(R.id.pt_type);

        ptMBudget = (EditText) findViewById(R.id.pt_mb);
        ptABudget = (EditText) findViewById(R.id.pt_ab);
        ptPaid = (EditText) findViewById(R.id.pt_paid);
        ptNote = (EditText) findViewById(R.id.pt_note);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ptAddition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((ptPtABudget.getVisibility() == View.INVISIBLE) && (ptABudget.getVisibility() == View.INVISIBLE)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Show Budget Addition");
                    alertDialog.setMessage("Are you sure ?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ptPtABudget.setVisibility(View.VISIBLE);
                                    ptABudget.setVisibility(View.VISIBLE);
                                    ptPtMBudget.setVisibility(View.INVISIBLE);
                                    ptMBudget.setVisibility(View.INVISIBLE);
                                    ptTotal.setText("0");
                                    ptMBudget.setText("0");
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Remove Budget Addition");
                    alertDialog.setMessage("Are you sure ?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ptPtABudget.setVisibility(View.INVISIBLE);
                                    ptABudget.setVisibility(View.INVISIBLE);
                                    ptABudget.setText("0");
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog.show();
                }
            }
        });

        ptMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((ptPtMBudget.getVisibility() == View.INVISIBLE) && (ptMBudget.getVisibility() == View.INVISIBLE)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Show Budget Reduce");
                    alertDialog.setMessage("Are you sure ?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ptPtMBudget.setVisibility(View.VISIBLE);
                                    ptMBudget.setVisibility(View.VISIBLE);
                                    ptPtABudget.setVisibility(View.INVISIBLE);
                                    ptABudget.setVisibility(View.INVISIBLE);
                                    ptTotal.setText("0");
                                    ptABudget.setText("0");
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Remove Budget Addition");
                    alertDialog.setMessage("Are you sure ?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ptPtMBudget.setVisibility(View.INVISIBLE);
                                    ptMBudget.setVisibility(View.INVISIBLE);
                                    ptMBudget.setText("0");
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog.show();
                }
            }
        });

        Intent intent = getIntent();
        final String id_p = intent.getStringExtra("id_product");
        final String id_c = intent.getStringExtra("id_customer");
        final String id_v = intent.getStringExtra("id_vendor");
        final String paxx = intent.getStringExtra("pax");

        ptName.setText(intent.getStringExtra("name"));
        ptNoHp.setText(intent.getStringExtra("no_hp"));
        ptOrder.setText(intent.getStringExtra("order"));
        ptCategory.setText(intent.getStringExtra("category"));
        ptPrice.setText(intent.getStringExtra("price"));
        ptType.setText(intent.getStringExtra("type"));
        ptOption.setText(intent.getStringExtra("option"));
        ptTotal.setText(intent.getStringExtra("price"));

        ptFinish = (Button) findViewById(R.id.pt_finish);

        ptMBudget.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ptTotal.setText(minnusNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ptPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ptTotal.setText(minnusNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ptABudget.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ptTotal.setText(addNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ptPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ptTotal.setText(addNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ptPaid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ptResidue.setText(minNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ptTotal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ptResidue.setText(minNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        final Api api = Retroserver.getClient().create(Api.class);

        ptFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
                String a_date = ptDate.getText().toString();
                if ((ptPaid.getText().toString().equals("")) && (a_date.equals("YYYY/MM/DD"))){
                    Toast.makeText(PackageTransaction.this, "Please Set Booking Date", Toast.LENGTH_LONG).show();
                    ptPaid.setError("Please Input Here!");
                    ptDate.setError("Please Set Booking Date!");
                } else {
                    final String minuss = ptResidue.getText().toString().trim();
                    if (a_date.equals("YYYY/MM/DD")) {
                        Toast.makeText(PackageTransaction.this, "Please Set Booking Date", Toast.LENGTH_LONG).show();
                    } else {
                        if (ptPaid.getText().toString().equals("")){
                            Toast.makeText(PackageTransaction.this, "Please Set Customer Paid", Toast.LENGTH_LONG).show();
                        } else {
                            if (Integer.valueOf(minuss) >= 0) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("Finish");
                                alertDialog.setMessage("Are you sure ?").setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Set up progress before call
                                                final ProgressDialog progressDoalog;
                                                progressDoalog = new ProgressDialog(PackageTransaction.this);
                                                progressDoalog.setMessage("Loading....");
                                                progressDoalog.setTitle("Progress");
                                                progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                                                // show it
                                                progressDoalog.show();
                                                progressDoalog.setCancelable(false);
                                                progressDoalog.setCanceledOnTouchOutside(false);

                                                final String id_product = id_p;
                                                final String id_customer = id_c;
                                                final String id_vendor = id_v;
                                                final String name = ptName.getText().toString().trim();
                                                final String no_hp = ptNoHp.getText().toString().trim();
                                                final String type = ptType.getText().toString().trim();
                                                final String order = ptOrder.getText().toString().trim();
                                                final String category = ptCategory.getText().toString().trim();
                                                final String price = ptPrice.getText().toString().trim();
                                                final String booking = ptDate.getText().toString().trim();
                                                final String option = ptOption.getText().toString().trim();
                                                final String note = ptNote.getText().toString().trim();
                                                final String paid = ptPaid.getText().toString().trim();
                                                final String minus = ptResidue.getText().toString().trim();
                                                final String total = ptTotal.getText().toString().trim();
                                                final String pax = paxx;

                                                Call<ResponModel> sendPT = api.sendPackageTransaction(id_product, id_customer, id_vendor, name, no_hp, order, category, option, price, type, booking, note, paid , minus, total, pax);
                                                sendPT.enqueue(new Callback<ResponModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                        progressDoalog.dismiss();
                                                        Intent i = new Intent(PackageTransaction.this, ListAcquittance.class);
                                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        Toast.makeText(getApplicationContext(), "Successfully", Toast.LENGTH_SHORT).show();
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                    @Override
                                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                                        Toast.makeText(PackageTransaction.this, "Error", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alertDialog1 = alertDialog.create();
                                alertDialog.show();
                            } else {
                                Toast.makeText(PackageTransaction.this, "Minus can't be negative number", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        ptDate.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        PackageTransaction.this,android.R.style.Theme_Holo_Dialog_MinWidth,
                        ptDateListener,
                        year,month,day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        ptDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String date = year + "/" + month + "/" + day;
                ptDate.setText(date);
            }
        };
    }

    private String minNumbers() {
        long number1;
        long number2;
        if(ptPaid.getText().toString() != "" && ptPaid.getText().length() > 0){
            number1 = Long.parseLong(ptPaid.getText().toString());
        } else {
            number1 = 0;
        }

        if (ptTotal.getText().toString() != "" && ptTotal.getText().length() > 0) {
            number2 = Long.parseLong(ptTotal.getText().toString());
        } else {
            number2 = 0;
        }
        return Long.toString(number2 - number1);
    }

    private String addNumbers() {
        long number1;
        long number2;
        if(ptABudget.getText().toString() != "" && ptABudget.getText().length() > 0){
            number1 = Long.parseLong(ptABudget.getText().toString());
        } else {
            number1 = 0;
        }

        if (ptPrice.getText().toString() != "" && ptPrice.getText().length() > 0) {
            number2 = Long.parseLong(ptPrice.getText().toString());
        } else {
            number2 = 0;
        }
        return Long.toString(number2 + number1);
    }

    private String minnusNumbers() {
        long number1;
        long number2;
        if(ptMBudget.getText().toString() != "" && ptMBudget.getText().length() > 0){
            number1 = Long.parseLong(ptMBudget.getText().toString());
        } else {
            number1 = 0;
        }

        if (ptPrice.getText().toString() != "" && ptPrice.getText().length() > 0) {
            number2 = Long.parseLong(ptPrice.getText().toString());
        } else {
            number2 = 0;
        }
        return Long.toString(number2 - number1);
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText ) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
