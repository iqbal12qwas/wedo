package com.app.wedo.transaction;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class RentTransaction extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "RentTransaction";

    @NotEmpty
    private EditText rtDuration, rtPaid, rtPeople;

    private Button rtFinish;

    final Context context = this;

    private EditText rtNote;
    private TextView rtDate, rtResidue, rtName, rtNoHp, rtOrder, rtCategory, rtPrice, rtTotal, rtType, rtTypeTime, rtTypeTime2, rtOption, rtTvPeople, rtCheckPax;
    private TextView rtTvDate;
    private DatePickerDialog.OnDateSetListener rtDateListener;

    private Validator validator;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_transsaction);
        setTitle("Rent Transaction");

        rtTvDate = (TextView) findViewById(R.id.rt_pt_booking);

        rtTvPeople = (TextView) findViewById(R.id.rt_pt_people);
        rtTvPeople.setVisibility(View.INVISIBLE);

        rtDate = (TextView) findViewById(R.id.rt_date);
        rtResidue = (TextView) findViewById(R.id.rt_residue);
        rtName = (TextView) findViewById(R.id.rt_name);
        rtNoHp = (TextView) findViewById(R.id.rt_no_hp);
        rtOrder = (TextView) findViewById(R.id.rt_order);
        rtCategory = (TextView) findViewById(R.id.rt_category);
        rtOption = (TextView) findViewById(R.id.rt_option);
        rtPrice = (TextView) findViewById(R.id.rt_price);
        rtTotal = (TextView) findViewById(R.id.rt_total);
        rtType = (TextView) findViewById(R.id.rt_type);
        rtTypeTime = (TextView) findViewById(R.id.rt_type_time);
        rtTypeTime2 = (TextView) findViewById(R.id.rt_type_time3);
        rtCheckPax = (TextView) findViewById(R.id.check_pax);

        rtDuration = (EditText) findViewById(R.id.rt_duration);
        rtPaid = (EditText) findViewById(R.id.rt_paid);
        rtNote = (EditText) findViewById(R.id.rt_note);
        rtPeople = (EditText) findViewById(R.id.rt_people);
        rtPeople.setVisibility(View.INVISIBLE);

        validator = new Validator(this);
        validator.setValidationListener(this);

        Intent intent = getIntent();
        final String id_p = intent.getStringExtra("id_product");
        final String id_c = intent.getStringExtra("id_customer");
        final String id_v = intent.getStringExtra("id_vendor");
        final String paxx = intent.getStringExtra("pax");

        rtName.setText(intent.getStringExtra("name"));
        rtNoHp.setText(intent.getStringExtra("no_hp"));
        rtOrder.setText(intent.getStringExtra("order"));
        rtCategory.setText(intent.getStringExtra("category"));
        rtPrice.setText(intent.getStringExtra("price"));
        rtType.setText(intent.getStringExtra("type"));
        rtOption.setText(intent.getStringExtra("option"));
        rtTypeTime.setText(intent.getStringExtra("type_duration"));
        rtTypeTime2.setText(intent.getStringExtra("type_duration"));
        rtCheckPax.setText(intent.getStringExtra("pax"));

        if ((rtCategory.getText().toString().equals("Wedding Venue")) && (rtCheckPax.getText().toString().equals("Yes"))){
            rtPeople.setVisibility(View.VISIBLE);
            rtTvPeople.setVisibility(View.VISIBLE);
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.BELOW, R.id.rt_relative2);
            params.addRule(RelativeLayout.END_OF, R.id.rt_pt_booking);
            params.setMarginStart(270);
            params.setMargins(0, 31, 0, 0);
            rtDate.setLayoutParams(params);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params2.addRule(RelativeLayout.BELOW, R.id.rt_relative2);
            params2.setMarginStart(45);
            params2.setMargins(0, 31, 0, 0);
            rtTvDate.setLayoutParams(params2);
        }

        rtFinish = (Button) findViewById(R.id.rt_finish);

        rtPeople.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rtTotal.setText(mulNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rtDuration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rtTotal.setText(mulNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rtTotal.setText(mulNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rtTotal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rtResidue.setText(minNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rtPaid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rtResidue.setText(minNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        final Api api = Retroserver.getClient().create(Api.class);

        rtFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
                String a_date = rtDate.getText().toString();
                if ((rtDuration.getText().toString().equals("")) && (rtPaid.getText().toString().equals("")) &&
                        (a_date.equals("YYYY/MM/DD")) && rtPeople.getText().toString().equals("")) {
                    Toast.makeText(RentTransaction.this, "Please Set Booking Date", Toast.LENGTH_LONG).show();
                    rtDuration.setError("Please Input Here!");
                    rtDate.setError("Please Set Booking Date!");
                    rtPaid.setError("Please Input Here!");
                    rtPeople.setError("Please Input Here!");
                } else {
                    if (a_date.equals("YYYY/MM/DD")) {
                        Toast.makeText(RentTransaction.this, "Please Set Booking Date", Toast.LENGTH_LONG).show();
                    } else {
                        if (rtDuration.getText().toString().equals("")) {
                            Toast.makeText(RentTransaction.this, "Please Set Duration", Toast.LENGTH_LONG).show();
                        } else {
                            if (rtPaid.getText().toString().equals("")) {
                                Toast.makeText(RentTransaction.this, "Please Set Customer Paid", Toast.LENGTH_LONG).show();
                            } else {
                                final String minuss = rtResidue.getText().toString().trim();
                                if (Integer.valueOf(minuss) >= 0) {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Finish");
                                    alertDialog.setMessage("Are you sure ?").setCancelable(false)
                                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Set up progress before call
                                                    final ProgressDialog progressDoalog;
                                                    progressDoalog = new ProgressDialog(RentTransaction.this);
                                                    progressDoalog.setMessage("Loading....");
                                                    progressDoalog.setTitle("Progress");
                                                    progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                                                    // show it
                                                    progressDoalog.show();
                                                    progressDoalog.setCancelable(false);
                                                    progressDoalog.setCanceledOnTouchOutside(false);

                                                    Intent intent = getIntent();
                                                    final String id_product = id_p;
                                                    final String id_customer = id_c;
                                                    final String id_vendor = id_v;
                                                    final String name = rtName.getText().toString().trim();
                                                    final String no_hp = rtNoHp.getText().toString().trim();
                                                    final String type = rtType.getText().toString().trim();
                                                    final String order = rtOrder.getText().toString().trim();
                                                    final String category = rtCategory.getText().toString().trim();
                                                    final String price = rtPrice.getText().toString().trim();
                                                    final String booking = rtDate.getText().toString().trim();
                                                    final String duration = rtDuration.getText().toString().trim();
                                                    final String option = rtOption.getText().toString().trim();
                                                    final String type_duration = rtTypeTime.getText().toString().trim();
                                                    final String note = rtNote.getText().toString().trim();
                                                    final String paid = rtPaid.getText().toString().trim();
                                                    final String minus = rtResidue.getText().toString().trim();
                                                    final String total = rtTotal.getText().toString().trim();
                                                    final String pax = paxx;
                                                    final String total_pax = rtPeople.getText().toString().trim();

                                                    Call<ResponModel> sendRT = api.sendRentTranssaction(id_product, id_customer, id_vendor, name, no_hp, order, category, option, price, type, booking, duration, type_duration, note, paid, minus, total, pax, total_pax);
                                                    sendRT.enqueue(new Callback<ResponModel>() {
                                                        @Override
                                                        public void onResponse(Call<ResponModel> call, retrofit2.Response<ResponModel> response) {
                                                            progressDoalog.dismiss();
                                                            Intent i = new Intent(RentTransaction.this, ListAcquittance.class);
                                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            Toast.makeText(getApplicationContext(), "Successfully", Toast.LENGTH_SHORT).show();
                                                            startActivity(i);
                                                            finish();
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ResponModel> call, Throwable t) {
                                                            Toast.makeText(RentTransaction.this, "Error", Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                }
                                            })
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });
                                    AlertDialog alertDialog1 = alertDialog.create();
                                    alertDialog.show();
                                } else {
                                    Toast.makeText(RentTransaction.this, "Minus can't be negative number", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
            }
        });

        rtDate.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        RentTransaction.this, android.R.style.Theme_Holo_Dialog_MinWidth,
                        rtDateListener,
                        year, month, day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        rtDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String date = year + "/" + month + "/" + day;
                rtDate.setText(date);
            }
        };
    }

    private String minNumbers() {
        long number1;
        long number2;
        if (rtPaid.getText().toString() != "" && rtPaid.getText().length() > 0) {
            number1 = Long.parseLong(rtPaid.getText().toString());
        } else {
            number1 = 0;
        }

        if (rtTotal.getText().toString() != "" && rtTotal.getText().length() > 0) {
            number2 = Long.parseLong(rtTotal.getText().toString());
        } else {
            number2 = 0;
        }

        return Long.toString(number2 - number1);
    }

    private String mulNumbers() {
        long number1, number2, number3;
        if (rtPrice.getText().toString() != "" && rtPrice.getText().length() > 0) {
            number1 = Long.parseLong(rtPrice.getText().toString());
        } else {
            number1 = 0;
        }

        if (rtDuration.getText().toString() != "" && rtDuration.getText().length() > 0) {
            number2 = Long.parseLong(rtDuration.getText().toString());
        } else {
            number2 = 0;
        }

        if (rtPeople.getText().toString() != "" && rtPeople.getText().length() > 0) {
            number3 = Long.parseLong(rtPeople.getText().toString());
        } else {
            number3 = 0;
        }
        return Long.toString(number1 * number2 * number3);
    }

    @Override
    public void onValidationSucceeded() {
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
