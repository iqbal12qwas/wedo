package com.app.wedo.transaction;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.wedo.ListAcquittance;
import com.app.wedo.R;
import com.app.wedo.api.Api;
import com.app.wedo.api.Retroserver;
import com.app.wedo.model.ResponModel;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellTransaction extends AppCompatActivity implements Validator.ValidationListener {

    private static final String TAG = "SellTransaction";

    @NotEmpty
    private EditText stUnit, stPaid;

    private Button stFinish;

    final Context context = this;

    private EditText stNote;
    private TextView stDate, stResidue, stName, stNoHp, stOrder, stCategory, stPrice, stTotal, stType, stOption, stStock;
    private DatePickerDialog.OnDateSetListener stDateListener;
    private ProgressDialog progressDialog;
    private Validator validator;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_transsaction);
        setTitle("Sell Transaction");

        stDate = (TextView) findViewById(R.id.pt_date);
        stResidue = (TextView) findViewById(R.id.pt_residue);
        stName = (TextView) findViewById(R.id.pt_name);
        stNoHp = (TextView) findViewById(R.id.pt_no_hp);
        stType = (TextView) findViewById(R.id.st_type);
        stOption = (TextView) findViewById(R.id.st_option);
        stOrder = (TextView) findViewById(R.id.pt_order);
        stCategory = (TextView) findViewById(R.id.pt_status);
        stPrice = (TextView) findViewById(R.id.pt_price);
        stTotal = (TextView) findViewById(R.id.pt_total);
        stStock = (TextView) findViewById(R.id.pt_stock);

        stUnit = (EditText) findViewById(R.id.st_unit);
        stPaid = (EditText) findViewById(R.id.pt_paid);
        stNote = (EditText) findViewById(R.id.pt_note);

        progressDialog = new ProgressDialog(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        Intent intent = getIntent();
        final String id_p = intent.getStringExtra("id_product");
        final String id_c = intent.getStringExtra("id_customer");
        final String id_v = intent.getStringExtra("id_vendor");
        final String paxx = intent.getStringExtra("pax");

        stName.setText(intent.getStringExtra("name"));
        stNoHp.setText(intent.getStringExtra("no_hp"));
        stOrder.setText(  intent.getStringExtra("order"));
        stCategory.setText(intent.getStringExtra("category"));
        stPrice.setText(intent.getStringExtra("price"));
        stType.setText(intent.getStringExtra("type"));
        stOption.setText(intent.getStringExtra("option"));
        stStock.setText(intent.getStringExtra("stock"));

        stFinish = (Button) findViewById(R.id.st_finish);

        stUnit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                stTotal.setText(mulNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        stPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                stTotal.setText(mulNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        stPaid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                stResidue.setText(minNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        stTotal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                stResidue.setText(minNumbers());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        final Api api = Retroserver.getClient().create(Api.class);


        stFinish.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                validator.validate();
                String a_date = stDate.getText().toString();
                if ((stUnit.getText().toString().equals("")) && (stPaid.getText().toString().equals("")) &&
                   (a_date.equals("YYYY/MM/DD"))){
                    Toast.makeText(SellTransaction.this, "Please Set Booking Date", Toast.LENGTH_LONG).show();
                    stUnit.setError("Please Input Here!");
                    stPaid.setError("Please Input Here!");
                    stDate.setError("Please Set Booking Date");
                } else {
                    if (a_date.equals("YYYY/MM/DD")) {
                        Toast.makeText(SellTransaction.this, "Please Set Booking Date", Toast.LENGTH_LONG).show();
                    } else {
                        if (stUnit.getText().toString().equals("")){
                            Toast.makeText(SellTransaction.this, "Please Set Unit", Toast.LENGTH_LONG).show();
                        } else {
                            if (stPaid.getText().toString().equals("")){
                                Toast.makeText(SellTransaction.this, "Please Set Customer Paid", Toast.LENGTH_LONG).show();
                            } else {
                                final String stock = stStock.getText().toString().trim();
                                final String unit = stUnit.getText().toString().trim();
                                if (Integer.valueOf(unit) <= Integer.valueOf(stock)){
                                    final String minuss = stResidue.getText().toString().trim();
                                    if (Integer.valueOf(minuss) >= 0) {
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                        alertDialog.setTitle("Finish");
                                        alertDialog.setMessage("Are you sure ?").setCancelable(false)
                                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // Set up progress before call
                                                        final ProgressDialog progressDoalog;
                                                        progressDoalog = new ProgressDialog(SellTransaction.this);
                                                        progressDoalog.setMessage("Loading....");
                                                        progressDoalog.setTitle("Progress");
                                                        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                                                        // show it
                                                        progressDoalog.show();
                                                        progressDoalog.setCancelable(false);
                                                        progressDoalog.setCanceledOnTouchOutside(false);

                                                        final String id_product = id_p;
                                                        final String id_customer = id_c;
                                                        final String id_vendor = id_v;
                                                        final String name = stName.getText().toString().trim();
                                                        final String no_hp = stNoHp.getText().toString().trim();
                                                        final String order = stOrder.getText().toString().trim();
                                                        final String option = stOption.getText().toString().trim();
                                                        final String category = stCategory.getText().toString().trim();
                                                        final String price = stPrice.getText().toString().trim();
                                                        final String type = stType.getText().toString().trim();
                                                        final String booking = stDate.getText().toString().trim();
                                                        final String unit = stUnit.getText().toString().trim();
                                                        final String note = stNote.getText().toString().trim();
                                                        final String paid = stPaid.getText().toString().trim();
                                                        final String minus = stResidue.getText().toString().trim();
                                                        final String total = stTotal.getText().toString().trim();
                                                        final String pax = paxx;
                                                        final String stockk = stStock.getText().toString().trim();
                                                        final int result = Integer.parseInt(stockk) - Integer.parseInt(unit);
                                                        final String stock = Integer.toString(result);

                                                        Call<ResponModel> sendST = api.sendSellTransaction(id_product, id_customer, id_vendor, name, no_hp, order, category, option, price, type, booking, unit, note, paid, minus, total, pax, stock);
                                                        sendST.enqueue(new Callback<ResponModel>() {
                                                            @Override
                                                            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                                progressDoalog.dismiss();
                                                                Intent i = new Intent(SellTransaction.this, ListAcquittance.class);
                                                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                Toast.makeText(getApplicationContext(), "Successfully", Toast.LENGTH_SHORT).show();
                                                                startActivity(i);
                                                                finish();
                                                            }

                                                            @Override
                                                            public void onFailure(Call<ResponModel> call, Throwable t) {
                                                                Toast.makeText(SellTransaction.this, "Error", Toast.LENGTH_LONG).show();
                                                            }
                                                        });
                                                    }
                                                })
                                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.cancel();
                                                    }
                                                });
                                        AlertDialog alertDialog1 = alertDialog.create();
                                        alertDialog.show();
                                    } else {
                                        Toast.makeText(SellTransaction.this, "Minus can't be negative number", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(SellTransaction.this, "Can't do Transaction, because limit Stock", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }
                }
            }
        });

        stDate.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        SellTransaction.this,android.R.style.Theme_Holo_Dialog_MinWidth,
                        stDateListener,
                        year,month,day);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        stDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: yyy/mm/dd: " + year + "/" + month + "/" + day);

                String date = year + "/" + month + "/" + day;
                stDate.setText(date);
            }
        };
    }

    private String minNumbers() {
        long number1, number2;
        if(stPaid.getText().toString() != "" && stPaid.getText().length() > 0){
            number1 = Long.parseLong(stPaid.getText().toString());
        } else {
            number1 = 0;
        }

        if (stTotal.getText().toString() != "" && stTotal.getText().length() > 0) {
            number2 = Long.parseLong(stTotal.getText().toString());
        } else {
            number2 = 0;
        }
        return Long.toString(number2 - number1);
    }

    private String mulNumbers() {
        long number1, number2;
        if(stUnit.getText().toString() != "" && stUnit.getText().length() > 0){
            number1 = Long.parseLong(stUnit.getText().toString());
        } else {
            number1 = 0;
        }

        if (stPrice.getText().toString() != "" && stPrice.getText().length() > 0) {
            number2 = Long.parseLong(stPrice.getText().toString());
        } else {
            number2 = 0;
        }
        return Long.toString(number1 * number2);
    }

    @Override
    public void onValidationSucceeded() {
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
